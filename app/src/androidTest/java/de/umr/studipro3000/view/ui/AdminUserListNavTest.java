package de.umr.studipro3000.view.ui;

import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.umr.studipro3000.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
/**
 * Test via Espresso: Tests the access of an admin to the UserList and its specific items
 */
public class AdminUserListNavTest extends LoginLogout {

  @Rule
  public ActivityTestRule<mainActivity> mActivityTestRule =
      new ActivityTestRule<>(mainActivity.class);

  @Test
  public void testAdminUserList() {

    login();

    ViewInteraction appCompatButton2 =
        onView(
            allOf(
                withId(R.id.adminActionsButton),
                withText("Admin"),
                childAtPosition(
                    allOf(
                        withId(R.id.toolbar), childAtPosition(withId(R.id.coordinatorLayout2), 0)),
                    2),
                isDisplayed()));
    appCompatButton2.perform(click());

    // This isn't a good way to tell Espresso to chill before clicking but it is one that works:
    SystemClock.sleep(500);

    ViewInteraction appCompatButton3 =
        onView(
            allOf(
                withId(R.id.buttonAdminUserList),
                withText("User List"),
                childAtPosition(childAtPosition(withId(R.id.nav_host_fragment), 0), 2),
                isDisplayed()));
    appCompatButton3.perform(click());

    ViewInteraction recyclerView =
        onView(
            allOf(
                withId(R.id.recycler_view_users),
                childAtPosition(withId(R.id.constraint_layout_users), 0)));
    recyclerView.perform(actionOnItemAtPosition(0, click()));

    // Verify Fragment
    ViewInteraction appCompatLayout = onView(allOf(withId(R.id.UserDetailsLayout), isDisplayed()));
    appCompatLayout.check(matches(isDisplayed()));

    logout();
  }

  private static Matcher<View> childAtPosition(
      final Matcher<View> parentMatcher, final int position) {

    return new TypeSafeMatcher<View>() {
      @Override
      public void describeTo(Description description) {
        description.appendText("Child at position " + position + " in parent ");
        parentMatcher.describeTo(description);
      }

      @Override
      public boolean matchesSafely(View view) {
        ViewParent parent = view.getParent();
        return parent instanceof ViewGroup
            && parentMatcher.matches(parent)
            && view.equals(((ViewGroup) parent).getChildAt(position));
      }
    };
  }
}
