package de.umr.studipro3000.view.ui;

import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.umr.studipro3000.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
public class LoginFailedTest {

  @Rule
  public ActivityTestRule<mainActivity> mActivityTestRule =
      new ActivityTestRule<>(mainActivity.class);

  /**
   * Automated test trough Espresso test recorder: Tests a login attempt with wrong username or
   * password
   */
  @Test
  public void loginFailedTest() {
    ViewInteraction appCompatEditText =
        onView(
            allOf(
                withId(R.id.LoginUserNameEmailEditText),
                isDisplayed()));
    appCompatEditText.perform(replaceText("t_admin"), closeSoftKeyboard());

    ViewInteraction appCompatEditText2 =
        onView(
            allOf(
                withId(R.id.LoginUserNameEmailEditText),
                withText("t_admin"),
                isDisplayed()));
    appCompatEditText2.perform(click());

    ViewInteraction appCompatEditText3 =
        onView(
            allOf(
                withId(R.id.LoginPasswordEditText),
                isDisplayed()));
    appCompatEditText3.perform(click());

    ViewInteraction appCompatEditText4 =
        onView(
            allOf(
                withId(R.id.LoginPasswordEditText),
                isDisplayed()));
    appCompatEditText4.perform(replaceText("false_password"));

    ViewInteraction appCompatEditText5 =
        onView(
            allOf(
                withId(R.id.LoginPasswordEditText),
                withText("false_password"),
                isDisplayed()));
    appCompatEditText5.perform(closeSoftKeyboard());

    ViewInteraction appCompatButton =
        onView(
            allOf(
                withId(R.id.loginButton),
                withText("Login"),
                isDisplayed()));
    appCompatButton.perform(click());
    // This isn't a good way to tell Espresso to chill after clicking but it is one that works:
    SystemClock.sleep(500);

    ViewInteraction textView =
        onView(
            allOf(
                withId(R.id.textView),
                withText("Die Kombination von Passwort und Email/Nutzername ist ungültig."),
                isDisplayed()));
    textView.check(
        matches(withText("Die Kombination von Passwort und Email/Nutzername ist ungültig.")));
  }
}
