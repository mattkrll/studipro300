package de.umr.studipro3000.view.ui;

import android.os.SystemClock;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.ViewInteraction;

import de.umr.studipro3000.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

public abstract class LoginLogout {

  void login() {
    ViewInteraction appCompatEditText =
            onView(allOf(withId(R.id.LoginUserNameEmailEditText), isDisplayed()));
    appCompatEditText.perform(click());

    ViewInteraction appCompatEditText2 =
            onView(allOf(withId(R.id.LoginUserNameEmailEditText), isDisplayed()));
    appCompatEditText2.perform(replaceText("t_admin"), closeSoftKeyboard());

    ViewInteraction appCompatEditText3 =
            onView(allOf(withId(R.id.LoginPasswordEditText), isDisplayed()));
    appCompatEditText3.perform(replaceText("test_admin_password"), closeSoftKeyboard());

    ViewInteraction appCompatButton =
            onView(allOf(withId(R.id.loginButton), withText("Login"), isDisplayed()));
    appCompatButton.perform(click());

    // This isn't a good way to tell Espresso to chill before clicking but it is one that works:
    SystemClock.sleep(500);
  }

  void logout() {
    Espresso.pressBack();
    Espresso.pressBack();
    Espresso.pressBack();
    ViewInteraction appComptButton2 = onView(allOf(withId(R.id.logOutButton), isDisplayed()));
    appComptButton2.perform(click());
  }
}
