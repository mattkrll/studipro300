package de.umr.studipro3000.view.ui;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.umr.studipro3000.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4ClassRunner.class)
public class LoginSuccessNavTest {

  /**
   * Test via Espresso: Tests a login attempt with correct username or
   * password
   */
  @Test
  public void LoginFragmentsNavTest() {
    // Setup
    ActivityScenario activityScenario = ActivityScenario.launch(mainActivity.class);

    // Navigate to Project Overview
    ViewInteraction appCompatEditText =
        onView(allOf(withId(R.id.LoginUserNameEmailEditText), isDisplayed()));
    appCompatEditText.perform(replaceText("t_admin"), closeSoftKeyboard());

    ViewInteraction appCompatEditText2 =
        onView(allOf(withId(R.id.LoginPasswordEditText), isDisplayed()));
    appCompatEditText2.perform(replaceText("test_admin_password"), closeSoftKeyboard());

    ViewInteraction appCompatButton =
        onView(allOf(withId(R.id.loginButton), withText("Login"), isDisplayed()));
    appCompatButton.perform(click());

    // Verify Fragment is now Project Overview
    ViewInteraction appCompatLayout = onView(allOf(withId(R.id.coordinatorLayout2), isDisplayed()));
    appCompatLayout.check(matches(isDisplayed()));

    ViewInteraction appComptButton2 =
            onView(allOf(withId(R.id.logOutButton),isDisplayed()));
    appComptButton2.perform(click());
  }
}
