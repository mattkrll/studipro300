package de.umr.studipro3000.view.ui;

import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.IdlingResource;
import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.umr.studipro3000.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
/**
 * Automated test trough Espresso test recorder: Tests if (with the right login credentials a user
 * can perform a click to open Projects form the Project overview
 */
public class ProjectDetailsNavTest extends LoginLogout {

  @Rule
  public ActivityTestRule<mainActivity> mActivityTestRule =
      new ActivityTestRule<>(mainActivity.class);

  @Test
  public void projectDetailsNavTest() {
   login();
    ViewInteraction recyclerView = onView(withId(R.id.recycler_view_projects));
    recyclerView.perform(actionOnItemAtPosition(0, click()));

    // Check Frame Layout vorhanden?
    ViewInteraction frameLayout = onView(withId(R.id.projectDetailsLayout));
    frameLayout.check(matches(isDisplayed()));

    Espresso.pressBack();
    ViewInteraction recyclerView2 = onView(withId(R.id.recycler_view_projects));
    recyclerView.perform(actionOnItemAtPosition(1, click()));

    ViewInteraction frameLayout5 = onView(withId(R.id.projectDetailsLayout));
    frameLayout.check(matches(isDisplayed()));

    logout();
  }

  private static Matcher<View> childAtPosition(
      final Matcher<View> parentMatcher, final int position) {

    return new TypeSafeMatcher<View>() {
      @Override
      public void describeTo(Description description) {
        description.appendText("Child at position " + position + " in parent ");
        parentMatcher.describeTo(description);
      }

      @Override
      public boolean matchesSafely(View view) {
        ViewParent parent = view.getParent();
        return parent instanceof ViewGroup
            && parentMatcher.matches(parent)
            && view.equals(((ViewGroup) parent).getChildAt(position));
      }
    };
  }
}
