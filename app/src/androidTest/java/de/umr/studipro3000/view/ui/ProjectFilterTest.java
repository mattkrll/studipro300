package de.umr.studipro3000.view.ui;

import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.umr.studipro3000.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasChildCount;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
/**
 * Test via Espresso: Tests if the Project Filter works and Filtered Words are shown, others are not
 */
public class ProjectFilterTest {

  @Rule
  public ActivityTestRule<mainActivity> mActivityTestRule =
      new ActivityTestRule<>(mainActivity.class);

  @Test
  public void projectFilterTest() {
    ViewInteraction appCompatEditText =
        onView(
            allOf(
                withId(R.id.LoginUserNameEmailEditText),
                childAtPosition(childAtPosition(withId(R.id.nav_host_fragment), 0), 1),
                isDisplayed()));
    appCompatEditText.perform(click());

    ViewInteraction appCompatEditText2 =
        onView(
            allOf(
                withId(R.id.LoginUserNameEmailEditText),
                childAtPosition(childAtPosition(withId(R.id.nav_host_fragment), 0), 1),
                isDisplayed()));
    appCompatEditText2.perform(replaceText("t_admin"), closeSoftKeyboard());

    ViewInteraction appCompatEditText3 =
        onView(
            allOf(
                withId(R.id.LoginPasswordEditText),
                childAtPosition(childAtPosition(withId(R.id.nav_host_fragment), 0), 2),
                isDisplayed()));
    appCompatEditText3.perform(replaceText("test_admin_password"), closeSoftKeyboard());

    ViewInteraction appCompatButton =
        onView(
            allOf(
                withId(R.id.loginButton),
                withText("Login"),
                childAtPosition(childAtPosition(withId(R.id.nav_host_fragment), 0), 3),
                isDisplayed()));
    appCompatButton.perform(click());

    // This isn't a good way to tell Espresso to chill before clicking but it is one that works:
    SystemClock.sleep(500);

    // Checks if there are two Projects prior to filtering
    ViewInteraction textView3 = onView(allOf(withId(R.id.recycler_view_projects), isDisplayed()));
    textView3.check(matches(hasChildCount(2)));

    ViewInteraction actionMenuItemView =
        onView(
            allOf(
                withId(R.id.search_bar),
                withContentDescription("search"),
                childAtPosition(childAtPosition(withId(R.id.toolbar), 4), 0),
                isDisplayed()));
    actionMenuItemView.perform(click());

    ViewInteraction appCompatImageView =
        onView(
            allOf(
                withClassName(is("androidx.appcompat.widget.AppCompatImageView")),
                withContentDescription("Search"),
                childAtPosition(
                    allOf(
                        withClassName(is("android.widget.LinearLayout")),
                        childAtPosition(withId(R.id.search_bar), 0)),
                    1),
                isDisplayed()));
    appCompatImageView.perform(click());

    ViewInteraction searchAutoComplete =
        onView(
            allOf(
                withClassName(is("android.widget.SearchView$SearchAutoComplete")),
                childAtPosition(
                    allOf(
                        withClassName(is("android.widget.LinearLayout")),
                        childAtPosition(withClassName(is("android.widget.LinearLayout")), 1)),
                    0),
                isDisplayed()));
    searchAutoComplete.perform(click());

    ViewInteraction searchAutoComplete2 =
        onView(
            allOf(
                withClassName(is("android.widget.SearchView$SearchAutoComplete")),
                childAtPosition(
                    allOf(
                        withClassName(is("android.widget.LinearLayout")),
                        childAtPosition(withClassName(is("android.widget.LinearLayout")), 1)),
                    0),
                isDisplayed()));
    searchAutoComplete2.perform(replaceText("test"), closeSoftKeyboard());

    ViewInteraction searchAutoComplete3 =
        onView(
            allOf(
                withClassName(is("android.widget.SearchView$SearchAutoComplete")),
                withText("test"),
                childAtPosition(
                    allOf(
                        withClassName(is("android.widget.LinearLayout")),
                        childAtPosition(withClassName(is("android.widget.LinearLayout")), 1)),
                    0),
                isDisplayed()));
    searchAutoComplete3.perform(pressImeActionButton());

    ViewInteraction textView =
        onView(
            allOf(
                withId(R.id.text_view_projectName),
                withText("Test_Projekt"),
                childAtPosition(
                    childAtPosition(
                        IsInstanceOf.<View>instanceOf(android.widget.FrameLayout.class), 0),
                    0),
                isDisplayed()));
    textView.check(matches(withText("Test_Projekt")));

    // Checks if there is realy only one Project left in the View
    ViewInteraction textView2 = onView(allOf(withId(R.id.recycler_view_projects), isDisplayed()));
    textView2.check(matches(hasChildCount(1)));

    Espresso.pressBack();
    ViewInteraction appComptButton2 =
            onView(allOf(withId(R.id.logOutButton),isDisplayed()));
    appComptButton2.perform(click());
  }

  private static Matcher<View> childAtPosition(
      final Matcher<View> parentMatcher, final int position) {

    return new TypeSafeMatcher<View>() {
      @Override
      public void describeTo(Description description) {
        description.appendText("Child at position " + position + " in parent ");
        parentMatcher.describeTo(description);
      }

      @Override
      public boolean matchesSafely(View view) {
        ViewParent parent = view.getParent();
        return parent instanceof ViewGroup
            && parentMatcher.matches(parent)
            && view.equals(((ViewGroup) parent).getChildAt(position));
      }
    };
  }
}
