package de.umr.studipro3000.view.ui;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
  LoginCodeButtonNavTest.class,
  LoginFailedTest.class,
  LoginSuccessNavTest.class,
  ProjectDetailsNavTest.class,
  AdminUserListNavTest.class,
  SetingsNavTest.class,
  ProjectFilterTest.class,
  UserFilterTest.class
})
public class TestSuit {}
