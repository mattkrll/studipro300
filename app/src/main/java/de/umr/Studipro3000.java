package de.umr;

import android.app.Application;
import android.content.Context;

public class Studipro3000 extends Application {
  private static Context context;

  public static Context getAppContext() {
    return Studipro3000.context;
  }

  public void onCreate() {
    super.onCreate();
    Studipro3000.context = getApplicationContext();
  }
}
