package de.umr.studipro3000.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

public class Appointment {
  @SerializedName("id")
  private int id;

  @SerializedName("name")
  private String name;

  @SerializedName("description")
  private String description;

  @SerializedName("startDate")
  private Date startDate;

  @SerializedName("endDate")
  private Date endDate;

  @SerializedName("type")
  private AppointmentType appointmentType;

  @SerializedName("reminders")
  private ArrayList<Reminder> reminders;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public AppointmentType getAppointmentType() {
    return appointmentType;
  }

  public void setAppointmentType(AppointmentType appointmentType) {
    this.appointmentType = appointmentType;
  }

  public ArrayList<Reminder> getReminders() {
    return reminders;
  }

  public void setReminders(ArrayList<Reminder> reminders) {
    this.reminders = reminders;
  }
}
