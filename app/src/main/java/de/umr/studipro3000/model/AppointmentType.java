package de.umr.studipro3000.model;

import com.google.gson.annotations.SerializedName;

public enum AppointmentType {
  @SerializedName("DEADLINE")
  DEADLINE,
  @SerializedName("MEETING")
  MEETING,
  @SerializedName("GROUP")
  GROUP,
  @SerializedName("TEMPLATE")
  TEMPLATE
}
