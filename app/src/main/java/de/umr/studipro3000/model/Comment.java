package de.umr.studipro3000.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Comment {
  @SerializedName("id")
  private int id;

  @SerializedName("content")
  private String content;

  @SerializedName("restricted")
  private boolean restricted;

  @SerializedName("wasEdited")
  private boolean wasEdited;

  @SerializedName("creationTime")
  private Date creationTiem;

  @SerializedName("author")
  private User author;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public boolean isRestricted() {
    return restricted;
  }

  public void setRestricted(boolean restricted) {
    this.restricted = restricted;
  }

  public boolean isWasEdited() {
    return wasEdited;
  }

  public void setWasEdited(boolean wasEdited) {
    this.wasEdited = wasEdited;
  }

  public Date getCreationTiem() {
    return creationTiem;
  }

  public void setCreationTiem(Date creationTiem) {
    this.creationTiem = creationTiem;
  }

  public User getAuthor() {
    return author;
  }

  public void setAuthor(User author) {
    this.author = author;
  }
}
