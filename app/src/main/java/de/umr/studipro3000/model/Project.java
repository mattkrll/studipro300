package de.umr.studipro3000.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Project {
  @SerializedName("id")
  private int id;

  @SerializedName("name")
  private String name;

  @SerializedName("description")
  private String description;

  @SerializedName("comments")
  private ArrayList<Comment> comments;

  @SerializedName("appointments")
  private ArrayList<Appointment> appointments;

  @SerializedName("handlers")
  private ArrayList<User> handlers;

  @SerializedName("processors")
  private ArrayList<User> processors;

  @SerializedName("type")
  private ProjectType projectType;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ArrayList<Comment> getComments() {
    return comments;
  }

  public void setComments(ArrayList<Comment> comments) {
    this.comments = comments;
  }

  public ArrayList<Appointment> getAppointments() {
    return appointments;
  }

  public void setAppointments(ArrayList<Appointment> appointments) {
    this.appointments = appointments;
  }

  public ArrayList<User> getHandlers() {
    return handlers;
  }

  public void setHandlers(ArrayList<User> handlers) {
    this.handlers = handlers;
  }

  public ArrayList<User> getProcessors() {
    return processors;
  }

  public void setProcessors(ArrayList<User> processors) {
    this.processors = processors;
  }

  public ProjectType getProjectType() {
    return projectType;
  }

  public void setProjectType(ProjectType projectType) {
    this.projectType = projectType;
  }
}
