package de.umr.studipro3000.model;

import com.google.gson.annotations.SerializedName;

public enum ProjectRights {
  @SerializedName("handler")
  HANDLER,
  @SerializedName("processor")
  PROCESSOR
}
