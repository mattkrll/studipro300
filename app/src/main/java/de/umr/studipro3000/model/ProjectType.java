package de.umr.studipro3000.model;

import com.google.gson.annotations.SerializedName;

public enum ProjectType {
  @SerializedName("FOPRA")
  FOPRA,
  @SerializedName("SEMINARARBEIT")
  SEMINARARBEIT,
  @SerializedName("BACHELORARBEIT")
  BACHELORARBEIT,
  @SerializedName("MASTERARBEIT")
  MASTERARBEIT,
  @SerializedName("PROJEKTARBEIT")
  PROJEKTARBEIT,
  @SerializedName("SWA")
  SWA
}
