package de.umr.studipro3000.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

public class Reminder {
  @SerializedName("id")
  private int id;

  @SerializedName("creator")
  private User creator;

  @SerializedName("remindTime")
  private Date remindTime;

  @SerializedName("reminderSubjects")
  private ArrayList<User> reminderSubjects;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public User getCreator() {
    return creator;
  }

  public void setCreator(User creator) {
    this.creator = creator;
  }

  public Date getRemindTime() {
    return remindTime;
  }

  public void setRemindTime(Date remindTime) {
    this.remindTime = remindTime;
  }

  public ArrayList<User> getReminderSubjects() {
    return reminderSubjects;
  }

  public void setReminderSubjects(ArrayList<User> reminderSubjects) {
    this.reminderSubjects = reminderSubjects;
  }
}
