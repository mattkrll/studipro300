package de.umr.studipro3000.model;

import android.content.Context;
import android.content.SharedPreferences;

import de.umr.Studipro3000;

public class SharedPreferencesService {

  private static SharedPreferencesService sharedPreferencesService;
  private final SharedPreferences sharedPreferences;

  public SharedPreferencesService(Context context) {
    sharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
  }

  // Singleton
  public static SharedPreferencesService getInstance() {
    if (sharedPreferencesService == null) {
      sharedPreferencesService = new SharedPreferencesService(Studipro3000.getAppContext());
    }
    return sharedPreferencesService;
  }

  public int getConnectionCheckInterval() {
    return sharedPreferences.getInt("connectionCheckInterval", 30);
  }

  public void setConnectionCheckInterval(int newConnectionInterval) {
    sharedPreferences.edit().putInt("connectionCheckInterval", newConnectionInterval).apply();
  }
}
