package de.umr.studipro3000.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Token {
  @NonNull @PrimaryKey public String accessToken;

  @ColumnInfo(name = "tokenType")
  public String tokenType;

  public Token(@NonNull String accessToken, @NonNull String tokenType) {
    this.accessToken = accessToken;
    this.tokenType = tokenType;
  }

  public String getTypeAndTokenForRequest() {
    return tokenType + " " + accessToken;
  }
}
