package de.umr.studipro3000.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

@Entity
public class User {
  @PrimaryKey
  @SerializedName("id")
  private int id;

  @SerializedName("username")
  private String username;

  @SerializedName("email")
  private String email;

  @SerializedName("roles")
  private ArrayList<UserRole> roles;

  @SerializedName("userInfo")
  private UserInfo userInfo;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  public UserInfo getUserInfo() {
    return userInfo;
  }

  public void setUserInfo(UserInfo userInfo) {
    this.userInfo = userInfo;
  }

  public ArrayList<UserRole> getRoles() {
    return roles;
  }

  public void setRoles(ArrayList<UserRole> roles) {
    this.roles = roles;
  }
}
