package de.umr.studipro3000.model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class UserInfo {
  @SerializedName("id")
  private int id;

  @Nullable
  @SerializedName("forename")
  private String forename;

  @Nullable
  @SerializedName("suranme")
  private String surname;

  @SerializedName("studentNumber")
  private int studentNumber;

  @Nullable
  @SerializedName("courseOfStudy")
  private String courseOfStudy;

  @Nullable
  @SerializedName("examinationRegulations")
  private String examinationRegulations;

  public UserInfo(
      int id,
      @Nullable String forename,
      @Nullable String surname,
      int studentNumber,
      @Nullable String courseOfStudy,
      @Nullable String examinationRegulations) {
    this.id = id;
    this.forename = forename;
    this.surname = surname;
    this.studentNumber = studentNumber;
    this.courseOfStudy = courseOfStudy;
    this.examinationRegulations = examinationRegulations;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Nullable
  public String getForename() {
    return forename;
  }

  public void setForename(@Nullable String forename) {
    this.forename = forename;
  }

  @Nullable
  public String getSurname() {
    return surname;
  }

  public void setSurname(@Nullable String surname) {
    this.surname = surname;
  }

  public int getStudentNumber() {
    return studentNumber;
  }

  public void setStudentNumber(int studentNumber) {
    this.studentNumber = studentNumber;
  }

  @Nullable
  public String getCourseOfStudy() {
    return courseOfStudy;
  }

  public void setCourseOfStudy(@Nullable String courseOfStudy) {
    this.courseOfStudy = courseOfStudy;
  }

  @Nullable
  public String getExaminationRegulations() {
    return examinationRegulations;
  }

  public void setExaminationRegulations(@Nullable String examinationRegulations) {
    this.examinationRegulations = examinationRegulations;
  }
}
