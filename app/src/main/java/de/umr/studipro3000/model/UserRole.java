package de.umr.studipro3000.model;

import com.google.gson.annotations.SerializedName;

public enum UserRole {
  @SerializedName("ROLE_USER")
  ROLE_USER,
  @SerializedName("ROLE_ADMIN")
  ROLE_ADMIN
}
