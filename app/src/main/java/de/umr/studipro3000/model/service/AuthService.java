package de.umr.studipro3000.model.service;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import de.umr.Studipro3000;
import de.umr.studipro3000.model.Token;
import de.umr.studipro3000.model.User;
import de.umr.studipro3000.model.service.db.AppDatabase;
import de.umr.studipro3000.model.service.db.TokenDao;
import de.umr.studipro3000.model.service.db.UserDao;
import de.umr.studipro3000.model.service.repository.Repository;
import de.umr.studipro3000.model.service.repository.requests.LoginRequest;
import de.umr.studipro3000.model.service.repository.responses.DefaultErrorResponse;
import de.umr.studipro3000.model.service.repository.responses.LoginResponse;
import de.umr.studipro3000.model.service.repository.responses.LoginSuccessResponse;
import de.umr.studipro3000.model.service.repository.responses.UserResponse;

public class AuthService {
  private static AuthService authService;
  private final Repository repository;
  private final TokenDao tokenDao;
  private final UserDao userDao;
  public MutableLiveData<Token> currentToken = new MutableLiveData<>();
  public MutableLiveData<User> currentUser = new MutableLiveData<>();
  public MutableLiveData<AuthenticationState> authenticationState = new MutableLiveData<>();
  public MutableLiveData<String> errorMessage = new MutableLiveData<>();

  public AuthService(Context context) {
    repository = Repository.getInstance();

    tokenDao = AppDatabase.getInstance(context).tokenDao();
    currentToken.setValue(tokenDao.getCurrentToken());

    userDao = AppDatabase.getInstance(context).userDao();
    currentUser.setValue(userDao.getCurrentUser());

    authenticationState.setValue(
        currentToken.getValue() != null && currentUser.getValue() != null
            ? AuthenticationState.AUTHENTICATED
            : AuthenticationState.UNAUTHENTICATED);
  }

  // Singleton
  public static AuthService getInstance() {
    if (authService == null) {
      authService = new AuthService(Studipro3000.getAppContext());
    }
    return authService;
  }

  private LoginRequest createLoginRequest(String usernameEmail, String password) {
    return android.util.Patterns.EMAIL_ADDRESS.matcher(usernameEmail).matches()
        ? new LoginRequest(null, usernameEmail, password)
        : new LoginRequest(usernameEmail, null, password);
  }

  public void signIn(String usernameEmail, String password) {
    // Make sure "there is no spoon1" ehm I wanted to say Token/User.
    tokenDao.deleteCurrentToken();
    userDao.deleteCurrentUser();
    errorMessage.setValue("");
    authenticationState.setValue(AuthenticationState.UNAUTHENTICATED);

    handleResponse(createLoginRequest(usernameEmail, password));
  }

  private void handleResponse(LoginRequest loginRequest) {
    LiveData<LoginResponse> loginResponseLiveData = repository.signIn(loginRequest);

    loginResponseLiveData.observeForever(
        loginResponse -> {
          LoginSuccessResponse loginSuccessResponse = loginResponse.getLoginSuccessResponse();

          if (loginResponse.getLoginSuccessResponse() != null) {
            Token responseToken =
                new Token(
                    loginSuccessResponse.getAccessToken(), loginSuccessResponse.getTokenType());

            currentToken.setValue(responseToken);
            tokenDao.insertToken(responseToken);

            // Login success, now get the User
            handleUserResponse(loginRequest);

          } else {
            DefaultErrorResponse loginErrorResponse = loginResponse.getDefaultErrorResponse();

            authenticationState.setValue(AuthenticationState.INVALID_AUTHENTICATION);
            errorMessage.setValue(loginErrorResponse.getError());
          }
        });
  }

  private void handleUserResponse(LoginRequest loginRequest) {
    LiveData<UserResponse> userResponseLiveData;
    if (loginRequest.getUsername() != null) {
      userResponseLiveData =
          repository.getUserByUsername(currentToken.getValue(), loginRequest.getUsername());
    } else {
      userResponseLiveData =
          repository.getUserByEmail(currentToken.getValue(), loginRequest.getEmail());
    }

    userResponseLiveData.observeForever(
        userResponse -> {
          User userResponseUser = userResponse.getUser();

          if (userResponseUser != null) {
            currentUser.setValue(userResponseUser);
            authenticationState.setValue(AuthenticationState.AUTHENTICATED);
          } else {
            DefaultErrorResponse userErrorResponse = userResponse.getDefaultErrorResponse();
            authenticationState.setValue(AuthenticationState.INVALID_AUTHENTICATION);
            errorMessage.setValue(userErrorResponse.getError());
          }
        });
  }

  public void signOut() {
    tokenDao.deleteCurrentToken();
    userDao.deleteCurrentUser();
    authenticationState.setValue(AuthenticationState.UNAUTHENTICATED);
  }

  public enum AuthenticationState {
    UNAUTHENTICATED, // Initial state, the user needs to authenticate
    AUTHENTICATED, // The user has authenticated successfully
    INVALID_AUTHENTICATION // Authentication failed
  }
}
