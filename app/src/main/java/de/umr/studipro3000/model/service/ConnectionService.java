package de.umr.studipro3000.model.service;

import android.app.AlarmManager;
import android.net.ConnectivityManager;

import de.umr.studipro3000.model.SharedPreferencesService;

public class ConnectionService {

  private AlarmManager alarmManager;
  private ConnectivityManager connectivityManager;
  private SharedPreferencesService sharedPreferencesService;

  public ConnectionService() {
    sharedPreferencesService = SharedPreferencesService.getInstance();
  }
}
