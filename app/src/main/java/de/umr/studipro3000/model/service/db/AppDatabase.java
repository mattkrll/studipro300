package de.umr.studipro3000.model.service.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import de.umr.studipro3000.model.Token;
import de.umr.studipro3000.model.User;

@Database(
    entities = {Token.class, User.class},
    version = 1,
    exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

  public abstract TokenDao tokenDao();

  public abstract UserDao userDao();

  private static AppDatabase INSTANCE;

  private static final Object sLock = new Object();

  public static AppDatabase getInstance(Context context) {
    synchronized (sLock) {
      if (INSTANCE == null) {
        INSTANCE =
            Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "token.db")
                .allowMainThreadQueries()
                .build();
      }
      return INSTANCE;
    }
  }
}
