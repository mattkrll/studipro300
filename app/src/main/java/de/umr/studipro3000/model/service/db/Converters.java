package de.umr.studipro3000.model.service.db;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import de.umr.studipro3000.model.UserInfo;
import de.umr.studipro3000.model.UserRole;

public class Converters {
  @TypeConverter
  public static UserRole toUserRole(int value) {
    return UserRole.values()[value];
  }

  @TypeConverter
  public static int fromUserRole(UserRole userRole) {
    return userRole.ordinal();
  }

  @TypeConverter
  public static ArrayList<UserRole> toRoles(String value) {
    Type userRolesListType = new TypeToken<ArrayList<UserRole>>() {}.getType();
    return new Gson().fromJson(value, userRolesListType);
  }

  @TypeConverter
  public static String fromRoles(ArrayList<UserRole> roles) {
    return new Gson().toJson(roles);
  }

  @TypeConverter
  public static UserInfo toUserInfo(String value) {
    return new Gson().fromJson(value, UserInfo.class);
  }

  @TypeConverter
  public static String fromUserInfo(UserInfo userinfo) {
    return new Gson().toJson(userinfo);
  }
}
