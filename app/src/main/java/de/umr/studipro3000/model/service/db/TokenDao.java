package de.umr.studipro3000.model.service.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import de.umr.studipro3000.model.Token;

@Dao
public interface TokenDao {
  @Query("SELECT accessToken, tokenType FROM token LIMIT 1")
  Token getCurrentToken();

  @Query("DELETE FROM token")
  void deleteCurrentToken();

  @Insert
  void insertToken(Token token);

  @Delete
  void deleteToken(Token token);
}
