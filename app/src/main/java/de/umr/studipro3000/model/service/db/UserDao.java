package de.umr.studipro3000.model.service.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import de.umr.studipro3000.model.User;

@Dao
public interface UserDao {
  @Query("SELECT id, username, email, roles, userinfo FROM user LIMIT 1")
  User getCurrentUser();

  @Query("DELETE FROM user")
  void deleteCurrentUser();

  @Insert
  void insertUser(User user);

  @Delete
  void deleteUser(User user);
}
