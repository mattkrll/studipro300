package de.umr.studipro3000.model.service.repository;

import de.umr.studipro3000.model.service.repository.requests.AddUserInvitationRequest;
import de.umr.studipro3000.model.service.repository.requests.CommentRequest;
import de.umr.studipro3000.model.service.repository.requests.CreateAppointmentRequest;
import de.umr.studipro3000.model.service.repository.requests.CreateByInviteCodeRequest;
import de.umr.studipro3000.model.service.repository.requests.DeleteUserRequest;
import de.umr.studipro3000.model.service.repository.requests.EditUserRequest;
import de.umr.studipro3000.model.service.repository.requests.LoginRequest;
import de.umr.studipro3000.model.service.repository.requests.ProjectRequest;
import de.umr.studipro3000.model.service.repository.requests.ReminderRequest;
import de.umr.studipro3000.model.service.repository.responses.AddUserInviteResponse;
import de.umr.studipro3000.model.service.repository.responses.AppointmentListResponse;
import de.umr.studipro3000.model.service.repository.responses.AppointmentResponse;
import de.umr.studipro3000.model.service.repository.responses.DefaultBackendResponse;
import de.umr.studipro3000.model.service.repository.responses.DeleteReminderResponse;
import de.umr.studipro3000.model.service.repository.responses.LoginResponse;
import de.umr.studipro3000.model.service.repository.responses.ProjectListResponse;
import de.umr.studipro3000.model.service.repository.responses.ProjectResponse;
import de.umr.studipro3000.model.service.repository.responses.ShowCommentsResponse;
import de.umr.studipro3000.model.service.repository.responses.UserListResponse;
import de.umr.studipro3000.model.service.repository.responses.UserResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface BackendAPI {
  // 1.2.1
  @POST("/api/auth/signin")
  Call<LoginResponse> signin(@Body LoginRequest loginRequest);

  // 1.3.1
  @POST("/api/project/init")
  Call<ProjectResponse> initProject(
      @Header("Authorization") String token, @Body ProjectRequest projectRequest);

  // 1.3.2
  @POST("/api/project/initFull")
  Call<ProjectResponse> fullinitProject(
      @Header("Authorization") String token, @Body ProjectRequest projectRequest);

  // 1.3.3
  @GET("/api/project")
  Call<ProjectListResponse> getAllProjects(@Header("Authorization") String token);

  // 1.3.4
  @GET("/api/project/")
  Call<ProjectListResponse> getProjectsOfUser(@Header("Authorization") String token);

  // 1.3.5
  @POST("/api/project/update/{id}")
  Call<ProjectResponse> updateProject(
      @Path("id") int id,
      @Header("Authorization") String token,
      @Body ProjectRequest projectRequest);

  // 1.3.6
  @DELETE("/api/project/delete/{id}")
  Call<ProjectResponse> deleteProject(@Path("id") int id, @Header("Authorization") String token);

  // 1.4.1
  @POST("/api/projects/{project_id}/appointments")
  Call<AppointmentResponse> createAppointmentInProject(
      @Path("project_id") int project_id,
      @Header("Authorization") String token,
      @Body CreateAppointmentRequest createAppointmentRequest);

  // 1.4.2
  @GET("/api/projects/{project_id}/appointments")
  Call<AppointmentListResponse> getAllApointmentsOfProject(
      @Path("project_id") int project_id, @Header("Authorization") String token);

  // 1.4.3
  @POST("/api/appointments/{appointment_id}")
  Call<AppointmentResponse> changeAppointment(
      @Path("appointment_id") int appointment_id,
      @Header("Authorization") String token,
      @Body CreateAppointmentRequest updatedAppointment);

  // 1.4.4
  @DELETE("/api/appointments/{appointment_id}")
  Call<DefaultBackendResponse> deleteAppointment(
      @Path("appointment_id") int appointmentId, @Header("Authorization") String token);

  // 1.5.1
  @POST("api/user/addUserInvitation")
  Call<AddUserInviteResponse> addUserInvitation(
      @Header("Authorization") String token,
      @Body AddUserInvitationRequest addUserInvitationRequest);

  // 1.5.2
  @POST("/api/user/byInvitation/{InvitationLink}")
  Call<DefaultBackendResponse> addUserByInviation(
      @Path("InvitationLink") String invitationLink,
      @Body CreateByInviteCodeRequest createByInviteCodeRequest);

  // 1.5.3
  @GET("/api/user/fetchall")
  Call<UserListResponse> getFetchAllUsers(@Header("Authorization") String token);

  // 1.5.4
  @POST("/api/user/editUserRequest")
  Call<DefaultBackendResponse> editUser(
      @Header("Authorization") String token, @Body EditUserRequest editUserRequest);

  // 1.5.5
  @GET("/api/user/getUserNameByEmail/{email}")
  Call<DefaultBackendResponse> getUserNameByEmail(
      @Header("Authorization") String token, @Path("email") String email);

  // 1.5.6
  @GET("/api/user/getUserByEmail/{email}")
  Call<UserResponse> getUserByEmail(
      @Header("Authorization") String token, @Path("email") String email);

  // 1.5.8
  @GET("/api/user/getUserByUserName/{username}")
  Call<UserResponse> getUserByUsername(
      @Header("Authorization") String token, @Path("username") String username);

  // 1.5.7
  // TODO: Warnung einblenden
  @DELETE("/api/user/deleteUserByEmailHardDelete")
  Call<DefaultBackendResponse> deleteUserByEmailHard(
      @Header("Authorization") String token, @Body DeleteUserRequest deleteUserRequest);

  // 1.5.8
  @DELETE("/api/user/deleteUserByEmail")
  Call<DefaultBackendResponse> deleteUserByEmail(
      @Header("Authorization") String token, @Body DeleteUserRequest deleteUserRequest);

  // 1.5.9
  @DELETE("/api/user/deleteSelf")
  Call<DefaultBackendResponse> deleteUserSelf(
      @Header("Authorization") String token, @Body DeleteUserRequest deleteUserRequest);

  // 1.6.1
  @GET("/api/projects/{project_id}/comments")
  Call<ShowCommentsResponse> showComments(
      @Header("Authorization") String token, @Path("project_id") int projectId);

  // 1.6.2
  @POST("/api/projects/{project_id}/comments")
  Call<DefaultBackendResponse> addComment(
      @Header("Authorization") String token,
      @Path("project_id") int projectId,
      @Body CommentRequest commentRequest);

  // 1.6.3
  @POST("/api/comments/{comment_id}")
  Call<DefaultBackendResponse> editComment(
      @Header("Authorization") String token,
      @Path("comment_id") int commentId,
      @Body CommentRequest commentRequest);

  // 1.6.4
  @DELETE("/api/comments/{comment_id}")
  Call<DefaultBackendResponse> deleteComment(
      @Header("Authorization") String token, @Path("comment_id") int commentId);

  // 1.7.1
  @GET("/api/reminder/{reminder_id}")
  Call<DefaultBackendResponse> getReminder(
      @Header("Authorization") String token, @Path("reminder_id") int reminderId);

  // 1.7.2
  @POST("/api/reminder/addReminder/{appointment_id}")
  Call<DefaultBackendResponse> addReminderToAppointment(
      @Header("Authorization") String token,
      @Path("appointment_id") int appointmentId,
      @Body ReminderRequest reminderRequest);

  // 1.7.3
  @POST("/api/reminder/edit/{reminder_id}")
  Call<DefaultBackendResponse> editReminder(
      @Header("Authorization") String token,
      @Path("reminder_id") long reminderId,
      @Body ReminderRequest reminderRequest);

  // 1.7.4
  @DELETE("/api/reminder/delete/{reminder_id}")
  Call<DeleteReminderResponse> deleteReminder(
      @Header("Authorization") String token, @Path("reminder_id") int reminderId);
}
