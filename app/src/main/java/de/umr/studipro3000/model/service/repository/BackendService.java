package de.umr.studipro3000.model.service.repository;

class BackendService {

  // private static final String HTTP_API_URL = "https://enoah98ojxcjp.x.pipedream.net/";
  private static final String HTTP_API_URL = "http://10.0.2.2:8081";

  public static BackendAPI getServiceClass() {
    return RetrofitAPI.getRetrofit(HTTP_API_URL).create(BackendAPI.class);
  }
}
