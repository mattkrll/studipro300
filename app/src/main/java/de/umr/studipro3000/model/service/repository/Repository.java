package de.umr.studipro3000.model.service.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.jetbrains.annotations.NotNull;

import de.umr.studipro3000.model.Token;
import de.umr.studipro3000.model.service.repository.requests.AddUserInvitationRequest;
import de.umr.studipro3000.model.service.repository.requests.CreateAppointmentRequest;
import de.umr.studipro3000.model.service.repository.requests.CreateByInviteCodeRequest;
import de.umr.studipro3000.model.service.repository.requests.EditUserRequest;
import de.umr.studipro3000.model.service.repository.requests.LoginRequest;
import de.umr.studipro3000.model.service.repository.requests.ProjectRequest;
import de.umr.studipro3000.model.service.repository.responses.AddUserInviteResponse;
import de.umr.studipro3000.model.service.repository.responses.AppointmentListResponse;
import de.umr.studipro3000.model.service.repository.responses.AppointmentResponse;
import de.umr.studipro3000.model.service.repository.responses.DefaultBackendResponse;
import de.umr.studipro3000.model.service.repository.responses.DefaultErrorResponse;
import de.umr.studipro3000.model.service.repository.responses.LoginResponse;
import de.umr.studipro3000.model.service.repository.responses.ProjectListResponse;
import de.umr.studipro3000.model.service.repository.responses.ProjectResponse;
import de.umr.studipro3000.model.service.repository.responses.ShowCommentsResponse;
import de.umr.studipro3000.model.service.repository.responses.UserListResponse;
import de.umr.studipro3000.model.service.repository.responses.UserResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository {

  private static Repository repository;
  private BackendAPI backendAPI;

  public Repository() {
    backendAPI = BackendService.getServiceClass();
  }

  // Singleton
  public static Repository getInstance() {
    if (repository == null) {
      repository = new Repository();
    }
    return repository;
  }

  // 1.2.1
  public LiveData<LoginResponse> signIn(final LoginRequest loginRequest) {
    final MutableLiveData<LoginResponse> loginResponseData = new MutableLiveData<>();
    System.out.println(loginRequest.getUsername());
    System.out.println(loginRequest.getEmail());

    backendAPI
        .signin(loginRequest)
        .enqueue(
            new Callback<LoginResponse>() {
              @Override
              public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {

                  loginResponseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(Call<LoginResponse> call, Throwable t) {
                loginResponseData.setValue(
                    new LoginResponse(null, new DefaultErrorResponse(t.getLocalizedMessage(), 0)));
              }
            });
    return loginResponseData;
  };

  // 1.3.1
  public LiveData<ProjectResponse> initProject(Token token, ProjectRequest projectRequest) {
    final MutableLiveData<ProjectResponse> responseData = new MutableLiveData<>();
    backendAPI
        .initProject(token.getTypeAndTokenForRequest(), projectRequest)
        .enqueue(
            new Callback<ProjectResponse>() {
              @Override
              public void onResponse(
                  @NotNull Call<ProjectResponse> call,
                  @NotNull Response<ProjectResponse> response) {
                if (response.isSuccessful()) {
                  responseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(@NotNull Call<ProjectResponse> call, @NotNull Throwable t) {
                responseData.setValue(
                    new ProjectResponse(
                        null, new DefaultErrorResponse(t.getLocalizedMessage(), 0)));
              }
            });
    return responseData;
  }

  // 1.3.2
  public LiveData<ProjectResponse> fullinitProject(Token token, ProjectRequest projectRequest) {
    final MutableLiveData<ProjectResponse> responseData = new MutableLiveData<>();
    backendAPI
        .initProject(token.getTypeAndTokenForRequest(), projectRequest)
        .enqueue(
            new Callback<ProjectResponse>() {
              @Override
              public void onResponse(
                  @NotNull Call<ProjectResponse> call,
                  @NotNull Response<ProjectResponse> response) {
                if (response.isSuccessful()) {
                  responseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(Call<ProjectResponse> call, Throwable t) {
                responseData.setValue(
                    new ProjectResponse(
                        null, new DefaultErrorResponse(t.getLocalizedMessage(), 0)));
              }
            });
    return responseData;
  }
  // 1.3.3
  public LiveData<ProjectListResponse> getAllprojects(Token token) {
    final MutableLiveData<ProjectListResponse> responseData = new MutableLiveData<>();

    backendAPI
        .getAllProjects(token.getTypeAndTokenForRequest())
        .enqueue(
            new Callback<ProjectListResponse>() {
              @Override
              public void onResponse(
                  Call<ProjectListResponse> call, Response<ProjectListResponse> response) {
                if (response.isSuccessful()) {
                  responseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(Call<ProjectListResponse> call, Throwable t) {
                responseData.setValue(
                    new ProjectListResponse(
                        null, new DefaultErrorResponse(t.getLocalizedMessage(), 0)));
              }
            });
    return responseData;
  }

  public LiveData<ProjectListResponse> getProjectsOfUser(Token token) {
    final MutableLiveData<ProjectListResponse> responseData = new MutableLiveData<>();

    backendAPI
        .getProjectsOfUser(token.getTypeAndTokenForRequest())
        .enqueue(
            new Callback<ProjectListResponse>() {
              @Override
              public void onResponse(
                  Call<ProjectListResponse> call, Response<ProjectListResponse> response) {
                if (response.isSuccessful()) {
                  responseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(Call<ProjectListResponse> call, Throwable t) {
                responseData.setValue(
                    new ProjectListResponse(
                        null, new DefaultErrorResponse(t.getLocalizedMessage(), 0)));
              }
            });
    return responseData;
  }

  // 1.3.5 /api/project/update/{id}
  public LiveData<ProjectResponse> update_id(
      int project_id, Token token, ProjectRequest projectRequest) {
    final MutableLiveData<ProjectResponse> responseData = new MutableLiveData<>();
    backendAPI
        .updateProject(project_id, token.getTypeAndTokenForRequest(), projectRequest)
        .enqueue(
            new Callback<ProjectResponse>() {
              @Override
              public void onResponse(
                  Call<ProjectResponse> call, Response<ProjectResponse> response) {
                if (response.isSuccessful()) {
                  responseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(Call<ProjectResponse> call, Throwable t) {
                responseData.setValue(
                    new ProjectResponse(
                        null, new DefaultErrorResponse(t.getLocalizedMessage(), 0)));
              }
            });
    return responseData;
  }

  // 1.3.6. /api/project/delete/{id}
  public LiveData<ProjectResponse> delete_id(int project_id, Token token) {
    final MutableLiveData<ProjectResponse> responseData = new MutableLiveData<>();
    backendAPI
        .deleteProject(project_id, token.getTypeAndTokenForRequest())
        .enqueue(
            new Callback<ProjectResponse>() {
              @Override
              public void onResponse(
                  Call<ProjectResponse> call, Response<ProjectResponse> response) {
                if (response.isSuccessful()) {
                  responseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(Call<ProjectResponse> call, Throwable t) {
                responseData.setValue(
                    new ProjectResponse(
                        null, new DefaultErrorResponse(t.getLocalizedMessage(), 0)));
              }
            });
    return responseData;
  }

  // 1.4.1 /api/projects/{project_id}/appointments
  public LiveData<AppointmentResponse> appointments(
      int project_id, Token token, CreateAppointmentRequest createAppointmentRequest) {
    final MutableLiveData<AppointmentResponse> responseData = new MutableLiveData<>();
    backendAPI
        .createAppointmentInProject(
            project_id, token.getTypeAndTokenForRequest(), createAppointmentRequest)
        .enqueue(
            new Callback<AppointmentResponse>() {
              @Override
              public void onResponse(
                  @NotNull Call<AppointmentResponse> call,
                  @NotNull Response<AppointmentResponse> response) {
                if (response.isSuccessful()) {
                  responseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(@NotNull Call<AppointmentResponse> call, @NotNull Throwable t) {
                responseData.setValue(
                    new AppointmentResponse(
                        null, new DefaultErrorResponse(t.getLocalizedMessage(), 0)));
              }
            });
    return responseData;
  }

  // 1.4.2 /api/projects/{project id}/appointments
  public LiveData<AppointmentListResponse> appointments(int project_id, Token token) {
    final MutableLiveData<AppointmentListResponse> responseData = new MutableLiveData<>();
    backendAPI
        .getAllApointmentsOfProject(project_id, token.getTypeAndTokenForRequest())
        .enqueue(
            new Callback<AppointmentListResponse>() {
              @Override
              public void onResponse(
                  Call<AppointmentListResponse> call, Response<AppointmentListResponse> response) {
                if (response.isSuccessful()) {
                  responseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(Call<AppointmentListResponse> call, Throwable t) {
                responseData.setValue(
                    new AppointmentListResponse(
                        null, null, new DefaultErrorResponse(t.getLocalizedMessage(), 0)));
              }
            });
    return responseData;
  }

  // 1.4.3 /api/appointments/{appointment_id}
  public LiveData<AppointmentResponse> appointment_id(
      int appointment_id, Token token, CreateAppointmentRequest updatedAppointment) {
    final MutableLiveData<AppointmentResponse> responseData = new MutableLiveData<>();
    backendAPI
        .changeAppointment(appointment_id, token.getTypeAndTokenForRequest(), updatedAppointment)
        .enqueue(
            new Callback<AppointmentResponse>() {
              @Override
              public void onResponse(
                  @NotNull Call<AppointmentResponse> call,
                  @NotNull Response<AppointmentResponse> response) {
                if (response.isSuccessful()) {
                  responseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(@NotNull Call<AppointmentResponse> call, @NotNull Throwable t) {
                responseData.setValue(
                    new AppointmentResponse(
                        null, new DefaultErrorResponse(t.getLocalizedMessage(), 0)));
              }
            });
    return responseData;
  }

  // 1.4.4 /api/appointments/{appointment_id}
  public LiveData<DefaultBackendResponse> appointment_id(int appointment_id, Token token) {
    final MutableLiveData<DefaultBackendResponse> responseData = new MutableLiveData<>();
    backendAPI
        .deleteAppointment(appointment_id, token.getTypeAndTokenForRequest())
        .enqueue(
            new Callback<DefaultBackendResponse>() {
              @Override
              public void onResponse(
                  Call<DefaultBackendResponse> call, Response<DefaultBackendResponse> response) {
                if (response.isSuccessful()) {
                  responseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(Call<DefaultBackendResponse> call, Throwable t) {
                responseData.setValue(new DefaultBackendResponse("0", t.getLocalizedMessage()));
              }
            });
    return responseData;
  }

  // 1.5.1
  public LiveData<AddUserInviteResponse> addUserInvitation(
      Token token, AddUserInvitationRequest addUserInvitationRequest) {
    final MutableLiveData<AddUserInviteResponse> responseData = new MutableLiveData<>();
    backendAPI
        .addUserInvitation(token.getTypeAndTokenForRequest(), addUserInvitationRequest)
        .enqueue(
            new Callback<AddUserInviteResponse>() {
              @Override
              public void onResponse(
                  @NotNull Call<AddUserInviteResponse> call,
                  @NotNull Response<AddUserInviteResponse> response) {
                if (response.isSuccessful()) {
                  responseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(
                  @NotNull Call<AddUserInviteResponse> call, @NotNull Throwable t) {
                responseData.setValue(null);
              }
            });
    return responseData;
  }

  // 1.5.2
  public LiveData<DefaultBackendResponse> addUserByInvitation(
      String invitationLink, CreateByInviteCodeRequest createByInviteCodeRequest) {
    final MutableLiveData<DefaultBackendResponse> responseData = new MutableLiveData<>();
    backendAPI
        .addUserByInviation(invitationLink, createByInviteCodeRequest)
        .enqueue(
            new Callback<DefaultBackendResponse>() {
              @Override
              public void onResponse(
                  @NotNull Call<DefaultBackendResponse> call,
                  @NotNull Response<DefaultBackendResponse> response) {
                if (response.isSuccessful()) {
                  responseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(
                  @NotNull Call<DefaultBackendResponse> call, @NotNull Throwable t) {
                responseData.setValue(new DefaultBackendResponse("0", t.getLocalizedMessage()));
              }
            });
    return responseData;
  }

  // 1.5.3
  public LiveData<UserListResponse> getFetchAllUsers(Token token) {
    final MutableLiveData<UserListResponse> responseData = new MutableLiveData<>();
    backendAPI
        .getFetchAllUsers(token.getTypeAndTokenForRequest())
        .enqueue(
            new Callback<UserListResponse>() {
              @Override
              public void onResponse(
                  Call<UserListResponse> call, Response<UserListResponse> response) {
                if (response.isSuccessful()) {
                  responseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(Call<UserListResponse> call, Throwable t) {
                responseData.setValue(
                    new UserListResponse(
                        null, null, new DefaultErrorResponse(t.getLocalizedMessage(), 0)));
              }
            });
    return responseData;
  }

  // 1.5.4
  public LiveData<DefaultBackendResponse> editUser(String token, EditUserRequest editUserRequest) {
    final MutableLiveData<DefaultBackendResponse> responseData = new MutableLiveData<>();
    backendAPI
        .editUser("Barer" + token, editUserRequest)
        .enqueue(
            new Callback<DefaultBackendResponse>() {
              @Override
              public void onResponse(
                  @NotNull Call<DefaultBackendResponse> call,
                  @NotNull Response<DefaultBackendResponse> response) {
                if (response.isSuccessful()) {
                  responseData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(
                  @NotNull Call<DefaultBackendResponse> call, @NotNull Throwable t) {
                responseData.setValue(null);
              }
            });
    return responseData;
  }

  // 1.5.6
  public LiveData<UserResponse> getUserByEmail(Token token, String email) {
    final MutableLiveData<UserResponse> userResponseMutableLiveData = new MutableLiveData<>();
    backendAPI
        .getUserByEmail(token.getTypeAndTokenForRequest(), email)
        .enqueue(
            new Callback<UserResponse>() {
              @Override
              public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.isSuccessful()) {
                  userResponseMutableLiveData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(Call<UserResponse> call, Throwable t) {
                userResponseMutableLiveData.setValue(
                    new UserResponse(null, new DefaultErrorResponse(t.getLocalizedMessage(), 0)));
              }
            });
    return userResponseMutableLiveData;
  };

  // 1.5.8
  public LiveData<UserResponse> getUserByUsername(Token token, String userName) {
    final MutableLiveData<UserResponse> userResponseMutableLiveData = new MutableLiveData<>();
    backendAPI
        .getUserByUsername(token.getTypeAndTokenForRequest(), userName)
        .enqueue(
            new Callback<UserResponse>() {
              @Override
              public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if (response.isSuccessful()) {
                  userResponseMutableLiveData.setValue(response.body());
                }
              }

              @Override
              public void onFailure(Call<UserResponse> call, Throwable t) {
                userResponseMutableLiveData.setValue(
                    new UserResponse(null, new DefaultErrorResponse(t.getLocalizedMessage(), 0)));
              }
            });
    return userResponseMutableLiveData;
  };

  // 1.6.1
  public LiveData<ShowCommentsResponse> showComments(Token token) {
    // TODO show Comments schreiben
    return null;
  }
}
