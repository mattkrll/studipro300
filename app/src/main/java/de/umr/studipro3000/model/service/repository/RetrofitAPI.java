package de.umr.studipro3000.model.service.repository;

import com.google.gson.GsonBuilder;

import de.umr.studipro3000.model.service.repository.deserializers.LoginResponseDeserializer;
import de.umr.studipro3000.model.service.repository.deserializers.ProjectListResponseDeserializer;
import de.umr.studipro3000.model.service.repository.deserializers.ProjectResponseDeserializer;
import de.umr.studipro3000.model.service.repository.deserializers.UserListResponseDeserializer;
import de.umr.studipro3000.model.service.repository.deserializers.UserResponseDeserializer;
import de.umr.studipro3000.model.service.repository.responses.LoginResponse;
import de.umr.studipro3000.model.service.repository.responses.ProjectListResponse;
import de.umr.studipro3000.model.service.repository.responses.ProjectResponse;
import de.umr.studipro3000.model.service.repository.responses.UserListResponse;
import de.umr.studipro3000.model.service.repository.responses.UserResponse;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitAPI {

  public static Retrofit getRetrofit(String url) {

    GsonBuilder gsonBuilder =
        new GsonBuilder()
            .registerTypeAdapter(LoginResponse.class, new LoginResponseDeserializer())
            .registerTypeAdapter(ProjectListResponse.class, new ProjectListResponseDeserializer())
            .registerTypeAdapter(ProjectResponse.class, new ProjectResponseDeserializer())
            .registerTypeAdapter(UserListResponse.class, new UserListResponseDeserializer())
            .registerTypeAdapter(UserResponse.class, new UserResponseDeserializer());

    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.level(HttpLoggingInterceptor.Level.BODY);
    OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

    return new Retrofit.Builder()
        .baseUrl(url)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
        .build();
  }
}
