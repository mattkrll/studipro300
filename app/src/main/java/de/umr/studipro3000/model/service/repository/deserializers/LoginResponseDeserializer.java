package de.umr.studipro3000.model.service.repository.deserializers;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import de.umr.studipro3000.model.service.repository.responses.DefaultErrorResponse;
import de.umr.studipro3000.model.service.repository.responses.LoginResponse;
import de.umr.studipro3000.model.service.repository.responses.LoginSuccessResponse;

public class LoginResponseDeserializer implements JsonDeserializer<LoginResponse> {

  @Override
  public LoginResponse deserialize(
      JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {

    final JsonObject jsonObject = json.getAsJsonObject();

    System.out.println(json.toString());

    if (jsonObject.has("Error")) {
      DefaultErrorResponse defaultErrorResponse =
          new Gson().fromJson(jsonObject, DefaultErrorResponse.class);

      return new LoginResponse(null, defaultErrorResponse);

    } else {
      LoginSuccessResponse loginSuccessResponse =
          new Gson().fromJson(jsonObject, LoginSuccessResponse.class);
      return new LoginResponse(loginSuccessResponse, null);
    }
  }
}
