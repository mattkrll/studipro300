package de.umr.studipro3000.model.service.repository.deserializers;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import de.umr.studipro3000.model.Project;
import de.umr.studipro3000.model.service.repository.responses.DefaultErrorResponse;
import de.umr.studipro3000.model.service.repository.responses.ProjectListResponse;
import de.umr.studipro3000.model.service.repository.responses.ProjectListSuccessResponse;

public class ProjectListResponseDeserializer implements JsonDeserializer<ProjectListResponse> {
  @Override
  public ProjectListResponse deserialize(
      JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {

    final JsonObject jsonObject = json.getAsJsonObject();
    if (jsonObject.has("Error")) {
      DefaultErrorResponse defaultErrorResponse =
          new Gson().fromJson(jsonObject, DefaultErrorResponse.class);

      return new ProjectListResponse(null, defaultErrorResponse);

    } else {

      Type projectListType = new TypeToken<List<Project>>() {}.getType();
      List<Project> projectList = new Gson().fromJson(jsonObject.get("result"), projectListType);
      ProjectListSuccessResponse projectListSuccessResponse =
          new ProjectListSuccessResponse(
              new Gson().fromJson(jsonObject.get("success"), Integer.class), projectList);

      return new ProjectListResponse(projectListSuccessResponse, null);
    }
  }
}
