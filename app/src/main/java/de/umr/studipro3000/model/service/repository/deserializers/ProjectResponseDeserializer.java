package de.umr.studipro3000.model.service.repository.deserializers;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import de.umr.studipro3000.model.service.repository.responses.ProjectResponse;
import de.umr.studipro3000.model.service.repository.responses.ProjectSuccessResponse;
import de.umr.studipro3000.model.service.repository.responses.DefaultErrorResponse;

public class ProjectResponseDeserializer implements JsonDeserializer<ProjectResponse> {

  @Override
  public ProjectResponse deserialize(
      JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {

    final JsonObject jsonObject = json.getAsJsonObject();
    if (jsonObject.has("Error")) {
      DefaultErrorResponse defaultErrorResponse =
          new Gson().fromJson(jsonObject, DefaultErrorResponse.class);

      return new ProjectResponse(null, defaultErrorResponse);

    } else {
      ProjectSuccessResponse projectSuccessResponse =
          new Gson().fromJson(jsonObject, ProjectSuccessResponse.class);
      return new ProjectResponse(projectSuccessResponse, null);
    }
  }
}
