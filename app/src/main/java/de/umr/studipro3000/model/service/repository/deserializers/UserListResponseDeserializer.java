package de.umr.studipro3000.model.service.repository.deserializers;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import de.umr.studipro3000.model.User;

import de.umr.studipro3000.model.service.repository.responses.UserListErrorResponse;
import de.umr.studipro3000.model.service.repository.responses.UserListResponse;

public class UserListResponseDeserializer implements JsonDeserializer<UserListResponse> {
  @Override
  public UserListResponse deserialize(
      JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {

    final JsonObject jsonObject = json.getAsJsonObject();
    if (jsonObject.has("Error")) {
      UserListErrorResponse userListErrorResponse =
          new Gson().fromJson(jsonObject, UserListErrorResponse.class);
      return new UserListResponse(null, userListErrorResponse, null);

    } else {

      Type userListType = new TypeToken<List<User>>() {}.getType();
      List<User> userList = new Gson().fromJson(jsonObject.get("result"), userListType);

      return new UserListResponse(userList, null, null);
    }
  }
}
