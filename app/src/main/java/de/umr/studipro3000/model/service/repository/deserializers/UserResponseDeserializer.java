package de.umr.studipro3000.model.service.repository.deserializers;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import de.umr.studipro3000.model.User;
import de.umr.studipro3000.model.service.repository.responses.DefaultErrorResponse;
import de.umr.studipro3000.model.service.repository.responses.UserResponse;

public class UserResponseDeserializer implements JsonDeserializer<UserResponse> {

  @Override
  public UserResponse deserialize(
      JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException {

    final JsonObject jsonObject = json.getAsJsonObject();

    System.out.println(json.toString());

    if (jsonObject.has("Error")) {
      DefaultErrorResponse defaultErrorResponse =
          new Gson().fromJson(jsonObject, DefaultErrorResponse.class);

      return new UserResponse(null, defaultErrorResponse);

    } else {
      User user = new Gson().fromJson(jsonObject, User.class);
      return new UserResponse(user, null);
    }
  }
}
