package de.umr.studipro3000.model.service.repository.requests;

import com.google.gson.annotations.SerializedName;

import de.umr.studipro3000.model.ProjectRights;
import de.umr.studipro3000.model.UserRole;

public class AddUserInvitationRequest {

  @SerializedName("email")
  private String email;

  @SerializedName("projectId")
  private int projectId;

  @SerializedName("role")
  private UserRole role;

  @SerializedName("projectRights")
  private ProjectRights projectRights;

  public AddUserInvitationRequest(
      String email, int projectId, UserRole role, ProjectRights projectRights) {
    this.email = email;
    this.projectId = projectId;
    this.role = role;
    this.projectRights = projectRights;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public int getProjectId() {
    return projectId;
  }

  public void setProjectId(int projectId) {
    this.projectId = projectId;
  }

  public UserRole getRole() {
    return role;
  }

  public void setRole(UserRole role) {
    this.role = role;
  }

  public ProjectRights getProjectRights() {
    return projectRights;
  }

  public void setProjectRights(ProjectRights projectRights) {
    this.projectRights = projectRights;
  }
}
