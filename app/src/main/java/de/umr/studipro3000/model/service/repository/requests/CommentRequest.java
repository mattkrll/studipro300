package de.umr.studipro3000.model.service.repository.requests;

import com.google.gson.annotations.SerializedName;

public class CommentRequest {
  @SerializedName("restricted")
  private boolean restricted;

  @SerializedName("content")
  private String content;

  public boolean isRestricted() {
    return restricted;
  }

  public void setRestricted(boolean restricted) {
    this.restricted = restricted;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }
}
