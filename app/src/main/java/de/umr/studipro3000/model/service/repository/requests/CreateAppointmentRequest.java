package de.umr.studipro3000.model.service.repository.requests;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import de.umr.studipro3000.model.Appointment;
import de.umr.studipro3000.model.AppointmentType;

public class CreateAppointmentRequest {
  @SerializedName("name")
  private String name;

  @SerializedName("description")
  private String description;

  @SerializedName("startDate")
  private String startDate;

  @SerializedName("endDate")
  private String endDate;

  @SerializedName("type")
  private AppointmentType type;

  public CreateAppointmentRequest(String name, @Nullable String description, String startDate, String endDate, AppointmentType type){
    this.name = name;
    this.description = description;
    this.startDate = startDate;
    this.endDate = endDate;
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate() {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }
}
