package de.umr.studipro3000.model.service.repository.requests;

import com.google.gson.annotations.SerializedName;

public class CreateByInviteCodeRequest {

  @SerializedName("forename")
  private String forename;

  @SerializedName("surname")
  private String surname;

  @SerializedName("studentNumber")
  private int studentNumber;

  @SerializedName("password")
  private String password;

  @SerializedName("username")
  private String username;

  @SerializedName("courseOfStudy")
  private String courseOfStudy;

  @SerializedName("examinationRegulations")
  private String examinationRegulations;

  public CreateByInviteCodeRequest(String forename, String surname, int studentNumber, String password, String username, String courseOfStudy, String examinationRegulations) {
    this.forename = forename;
    this.surname = surname;
    this.studentNumber = studentNumber;
    this.password = password;
    this.username = username;
    this.courseOfStudy = courseOfStudy;
    this.examinationRegulations = examinationRegulations;
  }

  public String getForename() {
    return forename;
  }

  public void setForename(String forename) {
    this.forename = forename;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public int getStudentNumber() {
    return studentNumber;
  }

  public void setStudentNumber(int studentNumber) {
    this.studentNumber = studentNumber;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getCourseOfStudy() {
    return courseOfStudy;
  }

  public void setCourseOfStudy(String courseOfStudy) {
    this.courseOfStudy = courseOfStudy;
  }

  public String getExaminationRegulations() {
    return examinationRegulations;
  }

  public void setExaminationRegulations(String examinationRegulations) {
    this.examinationRegulations = examinationRegulations;
  }
}
