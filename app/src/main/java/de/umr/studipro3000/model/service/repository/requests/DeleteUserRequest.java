package de.umr.studipro3000.model.service.repository.requests;

import com.google.gson.annotations.SerializedName;

public class DeleteUserRequest {
  @SerializedName("email")
  private String email;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
