package de.umr.studipro3000.model.service.repository.requests;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EditUserRequest {
  @SerializedName("userToEditByEmail")
  private String userToEditByEmail;

  @SerializedName("roles")
  private ArrayList<String> roles; // TODO: Arraylist hier richtig? ~Martin

  @SerializedName("email")
  private String email;

  @SerializedName("username")
  private String username;

  @SerializedName("forname")
  private String forname;

  @SerializedName("surname")
  private String surname;

  @SerializedName("studentNumber")
  private int studentNumber;

  @SerializedName("oldPassword")
  private String oldPassword;

  @SerializedName("newPassword")
  private String newPassword;

  public ArrayList<String> getRoles() {
    return roles;
  }

  public void setRoles(ArrayList<String> roles) {
    this.roles = roles;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getForname() {
    return forname;
  }

  public void setForname(String forname) {
    this.forname = forname;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public int getStudentNumber() {
    return studentNumber;
  }

  public void setStudentNumber(int studentNumber) {
    this.studentNumber = studentNumber;
  }

  public String getOldPassword() {
    return oldPassword;
  }

  public void setOldPassword(String oldPassword) {
    this.oldPassword = oldPassword;
  }

  public String getNewPassword() {
    return newPassword;
  }

  public void setNewPassword(String newPassword) {
    this.newPassword = newPassword;
  }

  public void setUserToEditByEmail(String userToEditByEmail) {
    this.userToEditByEmail = userToEditByEmail;
  }

  public String getUserToEditByEmail() {
    return userToEditByEmail;
  }
}
