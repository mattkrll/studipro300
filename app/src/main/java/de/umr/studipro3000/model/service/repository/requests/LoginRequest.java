package de.umr.studipro3000.model.service.repository.requests;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class LoginRequest {
  @Nullable
  @SerializedName("username")
  private String username;

  @Nullable
  @SerializedName("email")
  private String email;

  @SerializedName("password")
  private String password;

  public LoginRequest(@Nullable String username, @Nullable String email, String password) {
    this.username = username;
    this.email = email;
    this.password = password;
  }

  @Nullable
  public String getUsername() {
    return username;
  }

  public void setUsername(@Nullable String username) {
    this.username = username;
  }

  @Nullable
  public String getEmail() {
    return email;
  }

  public void setEmail(@Nullable String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
