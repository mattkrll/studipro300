package de.umr.studipro3000.model.service.repository.requests;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import de.umr.studipro3000.model.ProjectType;
import de.umr.studipro3000.model.User;

public class ProjectRequest {

  @SerializedName("name")
  private String name;

  @Nullable
  @SerializedName("description")
  private String description;

  @Nullable
  @SerializedName("handler")
  private ArrayList<User> handler;

  @Nullable
  @SerializedName("processor")
  private ArrayList<User> processor;

  @SerializedName("projectType")
  private ProjectType projectType;

  public ProjectRequest(
      String name,
      @Nullable String description,
      @Nullable ArrayList<User> handler,
      @Nullable ArrayList<User> processor,
      ProjectType projectType) {
    this.name = name;
    this.description = description;
    this.handler = handler;
    this.processor = processor;
    this.projectType = projectType;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ArrayList<User> getHandler() {
    return handler;
  }

  public void setHandler(ArrayList<User> handler) {
    this.handler = handler;
  }

  public ArrayList<User> getProcessor() {
    return processor;
  }

  public void setProcessor(ArrayList<User> processor) {
    this.processor = processor;
  }

  public ProjectType getProjectType() {
    return projectType;
  }

  public void setProjectType(ProjectType projectType) {
    this.projectType = projectType;
  }
}
