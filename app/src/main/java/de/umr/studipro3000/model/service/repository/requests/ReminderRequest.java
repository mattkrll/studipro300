package de.umr.studipro3000.model.service.repository.requests;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

public class ReminderRequest {
  @SerializedName("creator")
  private int creator;

  @SerializedName("reminderSubjects")
  private ArrayList<Integer> reminderSubjects;

  // TODO: Wo setze ich den default Wert?
  @Nullable
  @SerializedName("remindTime")
  private Date remindTime;

  public int getCreator() {
    return creator;
  }

  public void setCreator(int creator) {
    this.creator = creator;
  }

  public ArrayList<Integer> getReminderSubjects() {
    return reminderSubjects;
  }

  public void setReminderSubjects(ArrayList<Integer> reminderSubjects) {
    this.reminderSubjects = reminderSubjects;
  }

  @Nullable
  public Date getRemindTime() {
    return remindTime;
  }

  public void setRemindTime(@Nullable Date remindTime) {
    this.remindTime = remindTime;
  }
}
