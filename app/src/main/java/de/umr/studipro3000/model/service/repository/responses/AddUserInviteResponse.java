package de.umr.studipro3000.model.service.repository.responses;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class AddUserInviteResponse {
  @SerializedName("success")
  String success;

  @SerializedName("inviteLink")
  @Nullable
  String inviteLink;

  @Nullable
  @SerializedName("inviteLinkUrl")
  String inviteLinkUrl;

  @Nullable
  @SerializedName("Error")
  String error;

  @Nullable()
  @SerializedName("field")
  String field;

  public String getSuccess() {
    return success;
  }

  public void setSuccess(String success) {
    this.success = success;
  }

  @Nullable
  public String getInviteLink() {
    return inviteLink;
  }

  public void setInviteLink(@Nullable String inviteLink) {
    this.inviteLink = inviteLink;
  }

  @Nullable
  public String getInviteLinkUrl() {
    return inviteLinkUrl;
  }

  public void setInviteLinkUrl(@Nullable String inviteLinkUrl) {
    this.inviteLinkUrl = inviteLinkUrl;
  }

  @Nullable
  public String getError() {
    return error;
  }

  public void setError(@Nullable String error) {
    this.error = error;
  }

  @Nullable
  public String getField() {
    return field;
  }

  public void setField(@Nullable String field) {
    this.field = field;
  }
}
