package de.umr.studipro3000.model.service.repository.responses;

import com.google.gson.annotations.SerializedName;

public class AppointmentListErrorResponse {

  @SerializedName("timestamp")
  private String timestamp;

  @SerializedName("status")
  private int status;

  @SerializedName("error")
  private String error;

  @SerializedName("message")
  private String message;

  @SerializedName("path")
  private String path;

  public AppointmentListErrorResponse(
      String timestamp, int status, String error, String message, String path) {
    this.timestamp = timestamp;
    this.status = status;
    this.error = error;
    this.message = message;
    this.path = path;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }
}
