package de.umr.studipro3000.model.service.repository.responses;

import androidx.annotation.Nullable;

import java.util.List;

import de.umr.studipro3000.model.Appointment;

public class AppointmentListResponse {
  @Nullable private final List<Appointment> appointments;
  @Nullable private final AppointmentListErrorResponse appointmentErrorResponse;
  @Nullable private final DefaultErrorResponse defaultErrorResponse;

  public AppointmentListResponse(
      @Nullable List<Appointment> appointments,
      @Nullable AppointmentListErrorResponse appointmentErrorResponse,
      @Nullable DefaultErrorResponse defaultErrorResponse) {
    this.appointments = appointments;
    this.appointmentErrorResponse = appointmentErrorResponse;
    this.defaultErrorResponse = defaultErrorResponse;
  }

  @Nullable
  public List<Appointment> getAppointments() {
    return appointments;
  }

  @Nullable
  public DefaultErrorResponse getDefaultErrorResponse() {
    return defaultErrorResponse;
  }

  @Nullable
  public AppointmentListErrorResponse getAppointmentErrorResponse() {
    return appointmentErrorResponse;
  }
}
