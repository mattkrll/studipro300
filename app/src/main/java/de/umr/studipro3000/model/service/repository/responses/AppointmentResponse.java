package de.umr.studipro3000.model.service.repository.responses;

import androidx.annotation.Nullable;

public class AppointmentResponse {
  @Nullable private final AppointmentSuccessResponse appointmentSuccessResponse;
  @Nullable private final DefaultErrorResponse defaultErrorResponse;

  public AppointmentResponse(
      @Nullable AppointmentSuccessResponse appointmentSuccessResponse,
      @Nullable DefaultErrorResponse defaultErrorResponse) {
    this.appointmentSuccessResponse = appointmentSuccessResponse;
    this.defaultErrorResponse = defaultErrorResponse;
  }

  @Nullable
  public AppointmentSuccessResponse getAppointmentSuccessResponse() {
    return appointmentSuccessResponse;
  }

  @Nullable
  public DefaultErrorResponse getDefaultErrorResponse() {
    return defaultErrorResponse;
  }
}
