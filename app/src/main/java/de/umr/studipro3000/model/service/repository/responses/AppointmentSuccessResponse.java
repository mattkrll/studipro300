package de.umr.studipro3000.model.service.repository.responses;

import com.google.gson.annotations.SerializedName;

public class AppointmentSuccessResponse {
  @SerializedName("success")
  private int success;

  @SerializedName("appointment")
  private int appointment;

  public AppointmentSuccessResponse(int success, int appointment) {
    this.success = success;
    this.appointment = appointment;
  }

  public int getSuccess() {
    return success;
  }

  public void setSuccess(int success) {
    this.success = success;
  }

  public int getAppointment() {
    return appointment;
  }

  public void setAppointment(int appointment) {
    this.appointment = appointment;
  }
}
