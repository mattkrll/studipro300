package de.umr.studipro3000.model.service.repository.responses;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class DefaultBackendResponse {
  @SerializedName("success")
  private String success;

  @Nullable
  @SerializedName("Error")
  private String error;

  public DefaultBackendResponse(String success, @Nullable String error) {
    this.success = success;
    this.error = error;
  }

  @Nullable
  public String getError() {
    return error;
  }

  public void setError(@Nullable String error) {
    this.error = error;
  }

  public String getSuccess() {
    return success;
  }

  public void setSuccess(String success) {
    this.success = success;
  }
}
