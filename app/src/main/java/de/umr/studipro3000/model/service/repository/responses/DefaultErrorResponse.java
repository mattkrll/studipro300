package de.umr.studipro3000.model.service.repository.responses;

import com.google.gson.annotations.SerializedName;

public class DefaultErrorResponse {

  @SerializedName("Error")
  String error;

  @SerializedName("success")
  int success;

  public DefaultErrorResponse(String error, int success) {
    this.error = error;
    this.success = success;
  }

  public int getSuccess() {
    return success;
  }

  public void setSuccess(int success) {
    this.success = success;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }
}
