package de.umr.studipro3000.model.service.repository.responses;

import com.google.gson.annotations.SerializedName;

public class DeleteReminderResponse {
  @SerializedName("success")
  private int success;

  @SerializedName("Error")
  private String Error;

  @SerializedName("message")
  private String message;

  public int getSuccess() {
    return success;
  }

  public void setSuccess(int success) {
    this.success = success;
  }

  public String getError() {
    return Error;
  }

  public void setError(String error) {
    Error = error;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
