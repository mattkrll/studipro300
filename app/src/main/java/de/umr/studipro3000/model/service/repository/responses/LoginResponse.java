package de.umr.studipro3000.model.service.repository.responses;

import androidx.annotation.Nullable;

public class LoginResponse {
  @Nullable private final LoginSuccessResponse loginSuccessResponse;
  @Nullable private final DefaultErrorResponse defaultErrorResponse;

  public LoginResponse(
      @Nullable LoginSuccessResponse loginSuccessResponse,
      @Nullable DefaultErrorResponse defaultErrorResponse) {
    this.loginSuccessResponse = loginSuccessResponse;
    this.defaultErrorResponse = defaultErrorResponse;
  }

  @Nullable
  public LoginSuccessResponse getLoginSuccessResponse() {
    return loginSuccessResponse;
  }

  @Nullable
  public DefaultErrorResponse getDefaultErrorResponse() {
    return defaultErrorResponse;
  }
}
