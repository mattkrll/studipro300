package de.umr.studipro3000.model.service.repository.responses;

import com.google.gson.annotations.SerializedName;

public class LoginSuccessResponse {
  @SerializedName("tokenType")
  private String tokenType;

  @SerializedName("accessToken")
  private String accessToken;

  @SerializedName("success")
  private int success;

  public String getTokenType() {
    return tokenType;
  }

  public void setTokenType(String tokenType) {
    this.tokenType = tokenType;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public int getSuccess() {
    return success;
  }

  public void setSuccess(int success) {
    this.success = success;
  }
}
