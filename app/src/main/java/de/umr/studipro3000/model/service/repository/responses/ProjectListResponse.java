package de.umr.studipro3000.model.service.repository.responses;

import androidx.annotation.Nullable;

public class ProjectListResponse {
  @Nullable private final ProjectListSuccessResponse projectListSuccessResponse;
  @Nullable private final DefaultErrorResponse errorResponse;

  public ProjectListResponse(
      @Nullable ProjectListSuccessResponse projectListSuccessResponse,
      @Nullable DefaultErrorResponse errorResponse) {
    this.projectListSuccessResponse = projectListSuccessResponse;
    this.errorResponse = errorResponse;
  }

  @Nullable
  public ProjectListSuccessResponse getProjectListSuccessResponse() {
    return projectListSuccessResponse;
  }

  @Nullable
  public DefaultErrorResponse getErrorResponse() {
    return errorResponse;
  }
}
