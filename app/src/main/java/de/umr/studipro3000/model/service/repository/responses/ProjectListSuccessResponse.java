package de.umr.studipro3000.model.service.repository.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import de.umr.studipro3000.model.Project;

public class ProjectListSuccessResponse {
  @SerializedName("success")
  int success;

  @SerializedName("result")
  List<Project> result;

  public ProjectListSuccessResponse(int success, List<Project> result) {
    this.success = success;
    this.result = result;
  }

  public int getSuccess() {
    return success;
  }

  public void setSuccess(int success) {
    this.success = success;
  }

  public List<Project> getResult() {
    return result;
  }

  public void setResult(List<Project> result) {
    this.result = result;
  }
}
