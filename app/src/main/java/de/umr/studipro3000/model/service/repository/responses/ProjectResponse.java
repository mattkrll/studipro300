package de.umr.studipro3000.model.service.repository.responses;

import androidx.annotation.Nullable;

public class ProjectResponse {
  @Nullable private final ProjectSuccessResponse projectSuccessResponse;
  @Nullable private final DefaultErrorResponse defaultErrorResponse;

  public ProjectResponse(
      @Nullable ProjectSuccessResponse projectSuccessResponse,
      @Nullable DefaultErrorResponse defaultErrorResponse) {
    this.projectSuccessResponse = projectSuccessResponse;
    this.defaultErrorResponse = defaultErrorResponse;
  }

  @Nullable
  public DefaultErrorResponse getDefaultErrorResponse() {
    return defaultErrorResponse;
  }

  @Nullable
  public ProjectSuccessResponse getProjectSuccessResponse() {
    return projectSuccessResponse;
  }
}
