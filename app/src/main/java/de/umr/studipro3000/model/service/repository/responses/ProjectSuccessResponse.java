package de.umr.studipro3000.model.service.repository.responses;

import com.google.gson.annotations.SerializedName;

public class ProjectSuccessResponse {
  @SerializedName("success")
  private int success;

  @SerializedName("projectId")
  private int projectId;

  public ProjectSuccessResponse(int success, int projectId) {
    this.success = success;
    this.projectId = projectId;
  }

  public int getSuccess() {
    return success;
  }

  public void setSuccess(int success) {
    this.success = success;
  }

  public int getProjectId() {
    return projectId;
  }

  public void setProjectId(int projectId) {
    this.projectId = projectId;
  }
}
