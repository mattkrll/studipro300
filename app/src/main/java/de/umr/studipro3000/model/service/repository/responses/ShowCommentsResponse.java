package de.umr.studipro3000.model.service.repository.responses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import de.umr.studipro3000.model.Comment;

public class ShowCommentsResponse {
  @SerializedName("success")
  private int success;

  @SerializedName("Error")
  private String error;

  @SerializedName("comments")
  private ArrayList<Comment> comments;

  public int getSuccess() {
    return success;
  }

  public void setSuccess(int success) {
    this.success = success;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public ArrayList<Comment> getComments() {
    return comments;
  }

  public void setComments(ArrayList<Comment> comments) {
    this.comments = comments;
  }
}
