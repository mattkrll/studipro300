package de.umr.studipro3000.model.service.repository.responses;

import androidx.annotation.Nullable;

import java.util.List;

import de.umr.studipro3000.model.User;

public class UserListResponse {
  @Nullable private final List<User> users;
  @Nullable private final UserListErrorResponse userListErrorResponse;
  @Nullable private final DefaultErrorResponse errorResponse;

  public UserListResponse(
      @Nullable List<User> users,
      @Nullable UserListErrorResponse userListErrorResponse,
      @Nullable DefaultErrorResponse errorResponse) {
    this.users = users;
    this.userListErrorResponse = userListErrorResponse;
    this.errorResponse = errorResponse;
  }

  @Nullable
  public List<User> getUsers() {
    return users;
  }

  @Nullable
  public UserListErrorResponse getUserListErrorResponse() {
    return userListErrorResponse;
  }

  @Nullable
  public DefaultErrorResponse getErrorResponse() {
    return errorResponse;
  }
}
