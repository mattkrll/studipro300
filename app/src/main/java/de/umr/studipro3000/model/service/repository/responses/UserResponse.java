package de.umr.studipro3000.model.service.repository.responses;

import androidx.annotation.Nullable;

import de.umr.studipro3000.model.User;

public class UserResponse {
  @Nullable private final User user;
  @Nullable private final DefaultErrorResponse defaultErrorResponse;

  public UserResponse(@Nullable User user, @Nullable DefaultErrorResponse defaultErrorResponse) {
    this.user = user;
    this.defaultErrorResponse = defaultErrorResponse;
  }

  @Nullable
  public User getUser() {
    return user;
  }

  @Nullable
  public DefaultErrorResponse getDefaultErrorResponse() {
    return defaultErrorResponse;
  }
}
