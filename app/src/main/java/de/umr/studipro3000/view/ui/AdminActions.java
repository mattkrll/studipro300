package de.umr.studipro3000.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import de.umr.studipro3000.databinding.FragmentAdminActionsBinding;

public class AdminActions extends Fragment {
  private FragmentAdminActionsBinding binding;
  private NavController navController;

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    binding = FragmentAdminActionsBinding.inflate(inflater, container, false);
    binding.setLifecycleOwner(this);
    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    navController = Navigation.findNavController(view);

    binding.buttonInviteUser.setOnClickListener(
        v -> {
          navController.navigate(
              AdminActionsDirections.actionAdminActions2ToAdminInviteUserFragement());
        });

    binding.buttonAdminUserList.setOnClickListener(
        v -> {
          navController.navigate(
              AdminActionsDirections.actionAdminActions2ToAdminUserListFragment());
        });

    binding.buttonAdminCreateProject.setOnClickListener(
        v -> {
          navController.navigate(
              AdminActionsDirections.actionAdminActions2ToAdminCreateProjectFragment());
        });
  }
}
