package de.umr.studipro3000.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import de.umr.studipro3000.databinding.FragmentEnterInviteCodeAndNewUserDataBinding;
import de.umr.studipro3000.viewmodel.EnterInviteCodeAndUserDataViewModel;

public class EnterInviteCodeAndNewUserDataFragment extends Fragment {

  private EnterInviteCodeAndUserDataViewModel viewModel;
  private FragmentEnterInviteCodeAndNewUserDataBinding binding;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    viewModel = new ViewModelProvider(this).get(EnterInviteCodeAndUserDataViewModel.class);
  }

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {

    binding = FragmentEnterInviteCodeAndNewUserDataBinding.inflate(inflater, container, false);
    binding.setLifecycleOwner(this);
    binding.setEnterInviteCodeAndNewUserDataModel(viewModel);
    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    viewModel
        .getInviteCodeIsValid()
        .observe(
            getViewLifecycleOwner(),
            inviteCodeIsValid -> {
              if (inviteCodeIsValid) {

                Toast.makeText(getActivity(), "User created! Please login now!", Toast.LENGTH_SHORT)
                    .show();

                Navigation.findNavController(view).popBackStack();
              }
            });
  }
}
