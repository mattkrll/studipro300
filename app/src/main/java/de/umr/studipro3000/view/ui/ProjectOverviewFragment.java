package de.umr.studipro3000.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import de.umr.studipro3000.R;
import de.umr.studipro3000.databinding.FragmentProjectOverviewBinding;
import de.umr.studipro3000.model.Project;
import de.umr.studipro3000.model.UserRole;
import de.umr.studipro3000.viewmodel.ProjectAdapter;
import de.umr.studipro3000.viewmodel.ProjectOverViewNavigator;
import de.umr.studipro3000.viewmodel.ProjectOverviewViewModel;

public class ProjectOverviewFragment extends Fragment implements ProjectOverViewNavigator {

  private ProjectOverviewViewModel viewModel;
  private ProjectAdapter adapter;
  private FragmentProjectOverviewBinding binding;
  private NavController navController;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    viewModel = new ViewModelProvider(this).get(ProjectOverviewViewModel.class);
    viewModel.setNavigator(this);
    adapter = new ProjectAdapter();
  }

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    binding = FragmentProjectOverviewBinding.inflate(inflater, container, false);
    binding.setLifecycleOwner(this);
    binding.setProjectOverviewViewModel(viewModel);

    RecyclerView recyclerView = binding.recyclerViewProjects;
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.setHasFixedSize(true);
    adapter.setViewModel(viewModel);
    recyclerView.setAdapter(adapter);

    if (!viewModel.getCurrentUserRoles().contains(UserRole.ROLE_ADMIN)) {
      binding.adminActionsButton.setVisibility(View.GONE);
    }


    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    navController = Navigation.findNavController(view);

    viewModel
        .getProjectsLiveData()
        .observe(getViewLifecycleOwner(), projects -> adapter.setProjects(projects));

    binding.enterSettingsButton.setOnClickListener(
        v -> {
          navController.navigate(
              ProjectOverviewFragmentDirections.actionProjectOverviewFragmentToEditAppSettings());
        });
    binding.adminActionsButton.setOnClickListener(
        v -> {
          navController.navigate(
              ProjectOverviewFragmentDirections.actionProjectOverviewFragmentToAdminActions2());
        });

    binding.logOutButton.setOnClickListener(
        v -> {
          viewModel.logout();
          navController.navigate(
              ProjectOverviewFragmentDirections.actionProjectOverviewFragmentToLoginFragment());
        });
    ((mainActivity) getActivity()).setSupportActionBar(binding.toolbar);
    setHasOptionsMenu(true);
  }


    @Override
    public void onResume() {
        super.onResume();
        viewModel.updateData();
        adapter.getFilter().filter("");
    }

    @Override
  public void onItemClick(Project project) {

    ProjectOverviewFragmentDirections.ActionProjectOverviewFragmentToProjectDetailsFragment action =
        ProjectOverviewFragmentDirections.actionProjectOverviewFragmentToProjectDetailsFragment(
            new Gson().toJson(project, Project.class));

    navController.navigate(action);
  }

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    inflater.inflate(R.menu.search_menu, menu);
    MenuItem searchItem = menu.findItem(R.id.search_bar);
    SearchView searchView = (SearchView) searchItem.getActionView();
    searchView.setOnQueryTextListener(
        new SearchView.OnQueryTextListener() {
          @Override
          public boolean onQueryTextSubmit(String s) {
            return false;
          }

          @Override
          public boolean onQueryTextChange(String s) {
            adapter.getFilter().filter(s);
            return false;
          }
        });
    super.onCreateOptionsMenu(menu, inflater);
  }
}
