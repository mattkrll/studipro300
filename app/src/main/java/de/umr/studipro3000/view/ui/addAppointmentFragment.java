package de.umr.studipro3000.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.gson.Gson;

import de.umr.studipro3000.databinding.FragmentAddAppointmentBinding;
import de.umr.studipro3000.model.Project;
import de.umr.studipro3000.viewmodel.AddAppointmentViewModel;

public class addAppointmentFragment extends Fragment {
  private AddAppointmentViewModel viewModel;
  private FragmentAddAppointmentBinding binding;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    viewModel = new ViewModelProvider(this).get(AddAppointmentViewModel.class);
    addAppointmentFragmentArgs args = addAppointmentFragmentArgs.fromBundle(getArguments());
    viewModel.setProject(new Gson().fromJson(args.getProject(), Project.class));
  }

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    binding = FragmentAddAppointmentBinding.inflate(inflater, container, false);
    binding.setLifecycleOwner(this);
    binding.setAddAppointmentModel(viewModel);
    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    final NavController navController = Navigation.findNavController(view);

    binding.createNewAppointment.setOnClickListener(
        v -> {
          viewModel.createAppointment();
        });
    viewModel
        .getDataValid()
        .observe(
            getViewLifecycleOwner(),
            dataValid -> {
              if (dataValid) {
                navController.popBackStack();
              }
            });
  }
}
