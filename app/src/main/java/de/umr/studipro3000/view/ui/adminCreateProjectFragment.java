package de.umr.studipro3000.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import java.util.ArrayList;

import de.umr.studipro3000.databinding.FragmentAdminCreateProjectBinding;
import de.umr.studipro3000.model.User;
import de.umr.studipro3000.viewmodel.AdminCreateProjectViewModel;

public class adminCreateProjectFragment extends Fragment {
  private AdminCreateProjectViewModel viewModel;
  private FragmentAdminCreateProjectBinding binding;
  private ArrayList<User> handlers;
  private ArrayList<User> processors;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    viewModel = new ViewModelProvider(this).get(AdminCreateProjectViewModel.class);
  }

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    binding = FragmentAdminCreateProjectBinding.inflate(inflater, container, false);
    binding.setLifecycleOwner(this);
    binding.setAdminCreateProjectViewModel(viewModel);
    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    final NavController navController = Navigation.findNavController(view);
    handlers = new ArrayList<>();
    processors = new ArrayList<>();

    binding.createProject.setOnClickListener(
        v -> {
          viewModel.createProject();
        });

    viewModel
        .getProjectDataValid()
        .observe(
            getViewLifecycleOwner(),
            projectDataValid -> {
              if (projectDataValid) {
                // TODO: Feedback über angelegtes Projekt
                navController.popBackStack();
              }
            });
  }
}
