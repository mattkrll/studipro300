package de.umr.studipro3000.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import de.umr.studipro3000.databinding.FragmentAdminInviteUserFragementBinding;
import de.umr.studipro3000.viewmodel.AdminInviteUserViewModel;

public class adminInviteUserFragement extends Fragment {
  private AdminInviteUserViewModel viewModel;
  private FragmentAdminInviteUserFragementBinding binding;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    viewModel = new ViewModelProvider(this).get(AdminInviteUserViewModel.class);
  }

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    binding = FragmentAdminInviteUserFragementBinding.inflate(inflater, container, false);
    binding.setLifecycleOwner(this);
    binding.setAdminInviteUserViewModel(viewModel);

    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    final NavController navController = Navigation.findNavController(view);

    binding.button4.setOnClickListener(
        v -> {
          viewModel.inviteUser();
        });

    viewModel
        .getUserInviteIsValid()
        .observe(
            getViewLifecycleOwner(),
            userInviteIsValid -> {
              if (userInviteIsValid) {
                // TODO: Feedback über Ivite erfolgreich
                navController.popBackStack();
              }
            });
  }
}
