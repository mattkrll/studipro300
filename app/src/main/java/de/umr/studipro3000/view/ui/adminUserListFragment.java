package de.umr.studipro3000.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import de.umr.studipro3000.R;
import de.umr.studipro3000.databinding.FragmentAdminUserListBinding;
import de.umr.studipro3000.model.User;
import de.umr.studipro3000.viewmodel.AdminUserListNavigatior;
import de.umr.studipro3000.viewmodel.AdminUserListViewModel;
import de.umr.studipro3000.viewmodel.UserAdapter;

public class adminUserListFragment extends Fragment implements AdminUserListNavigatior {
  private AdminUserListViewModel viewModel;
  private UserAdapter adapter;
  private FragmentAdminUserListBinding binding;
  private NavController navController;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    viewModel = new ViewModelProvider(this).get(AdminUserListViewModel.class);
    viewModel.setNavigator(this);
    adapter = new UserAdapter();
  }

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    binding = FragmentAdminUserListBinding.inflate(inflater, container, false);
    binding.setLifecycleOwner(this);
    binding.setAdminUserListViewModel(viewModel);

    RecyclerView recyclerView = binding.recyclerViewUsers;
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.setHasFixedSize(true);
    adapter.setViewModel(viewModel);
    recyclerView.setAdapter(adapter);
    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    navController = Navigation.findNavController(view);

    viewModel
        .getUserListLiveData()
        .observe(getViewLifecycleOwner(), users -> adapter.setUsers(users));
    ((mainActivity) getActivity()).setSupportActionBar(binding.toolbar2);
    setHasOptionsMenu(true);
  };


  @Override
  public void onResume() {
    super.onResume();
    viewModel.updateData();
    adapter.getFilter().filter("");
  }

  @Override
  public void onItemClick(User user) {
    navController.navigate(
        adminUserListFragmentDirections.actionAdminUserListFragmentToUserDetailsFragment(
            new Gson().toJson(user)));
  }

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    inflater.inflate(R.menu.search_menu, menu);
    MenuItem searchItem = menu.findItem(R.id.search_bar);
    SearchView searchView = (SearchView) searchItem.getActionView();
    searchView.setOnQueryTextListener(
        new SearchView.OnQueryTextListener() {
          @Override
          public boolean onQueryTextSubmit(String s) {
            return false;
          }

          @Override
          public boolean onQueryTextChange(String s) {
            adapter.getFilter().filter(s);
            return false;
          }
        });
    super.onCreateOptionsMenu(menu, inflater);
  }
}
