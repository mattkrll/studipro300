package de.umr.studipro3000.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.gson.Gson;

import de.umr.studipro3000.databinding.FragmentEditAppointmentBinding;
import de.umr.studipro3000.model.Appointment;
import de.umr.studipro3000.viewmodel.EditAppointmentViewModel;
import de.umr.studipro3000.viewmodel.EditAppointmentViewModelFactory;

public class editAppointmentFragment extends Fragment {
  private EditAppointmentViewModel viewModel;
  private FragmentEditAppointmentBinding binding;
  private EditAppointmentViewModelFactory fac;
  private Appointment appointment;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    editAppointmentFragmentArgs args = editAppointmentFragmentArgs.fromBundle(getArguments());
    appointment = new Gson().fromJson(args.getAppointment(), Appointment.class);
    fac = new EditAppointmentViewModelFactory(appointment);
    viewModel = new ViewModelProvider(this, fac).get(EditAppointmentViewModel.class);
  }

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    binding = FragmentEditAppointmentBinding.inflate(inflater, container, false);
    binding.setLifecycleOwner(this);
    binding.setEditAppointmentModel(viewModel);
    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    final NavController navController = Navigation.findNavController(view);
    binding.editAppointment.setOnClickListener(
        v -> {
          viewModel.editAppointment();
        });
    binding.deleteAppointment.setOnClickListener(
        v -> {
          viewModel.deleteAppointment();
        });
    viewModel
        .getDataValid()
        .observe(
            getViewLifecycleOwner(),
            dataValid -> {
              if (dataValid) {
                navController.popBackStack();
              }
            });
  }
}
