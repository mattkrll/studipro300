package de.umr.studipro3000.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.gson.Gson;

import de.umr.studipro3000.databinding.FragmentEditProjectBinding;
import de.umr.studipro3000.model.Project;
import de.umr.studipro3000.viewmodel.EditProjectViewModel;
import de.umr.studipro3000.viewmodel.EditProjectViewModelFactory;

public class editProjectFragment extends Fragment {
  private EditProjectViewModel viewModel;
  private FragmentEditProjectBinding binding;
  private EditProjectViewModelFactory fac;
  private Project project;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    editProjectFragmentArgs args = editProjectFragmentArgs.fromBundle(getArguments());
    project = new Gson().fromJson(args.getProject(), Project.class);
    fac = new EditProjectViewModelFactory(project);
    viewModel = new ViewModelProvider(this, fac).get(EditProjectViewModel.class);
  }

  @Override
  public View onCreateView(
      LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    binding = FragmentEditProjectBinding.inflate(inflater, container, false);
    binding.setLifecycleOwner(this);
    binding.setEditProjectViewModel(viewModel);
    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    final NavController navController = Navigation.findNavController(view);
    binding.editProject.setOnClickListener(
        v -> {
          viewModel.editProject();
        });
    viewModel
        .getProjectDataValid()
        .observe(
            getViewLifecycleOwner(),
            projectDataValid -> {
              if (projectDataValid) {
                navController.popBackStack();
              }
            });
  }
}
