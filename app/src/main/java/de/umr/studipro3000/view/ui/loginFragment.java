package de.umr.studipro3000.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import de.umr.studipro3000.R;
import de.umr.studipro3000.databinding.FragmentLoginBinding;
import de.umr.studipro3000.viewmodel.LoginViewModel;

public class loginFragment extends Fragment {
  private LoginViewModel viewModel;
  private FragmentLoginBinding binding;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    viewModel = new ViewModelProvider(this).get(LoginViewModel.class);
  }

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {

    binding = FragmentLoginBinding.inflate(inflater, container, false);
    binding.setLifecycleOwner(this);
    binding.setLoginViewModel(viewModel);
    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    final NavController navController = Navigation.findNavController(view);

    viewModel
        .getAuthenticationState()
        .observe(
            getViewLifecycleOwner(),
            authenticationState -> {
              switch (authenticationState) {
                case AUTHENTICATED:
                  navController.navigate(R.id.action_loginFragment_to_projectOverviewFragment);
                  break;
                case INVALID_AUTHENTICATION:
                  break;
              }
            });

    binding.buttonInvite.setOnClickListener(
        v ->
            navController.navigate(
                R.id.action_loginFragment_to_enterInviteCodeAndNewUserDataFragment));
  }
}
