package de.umr.studipro3000.view.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import de.umr.studipro3000.R;

public class mainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }
}
