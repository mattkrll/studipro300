package de.umr.studipro3000.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;

import de.umr.studipro3000.databinding.FragmentProjectDetailsBinding;
import de.umr.studipro3000.model.Appointment;
import de.umr.studipro3000.model.Project;
import de.umr.studipro3000.viewmodel.AppointmentAdapter;
import de.umr.studipro3000.viewmodel.CommentAdapter;
import de.umr.studipro3000.viewmodel.ProjectDetailsNavigator;
import de.umr.studipro3000.viewmodel.ProjectDetailsViewModel;
import de.umr.studipro3000.viewmodel.ProjectDetailsViewModelFactory;

public class projectDetailsFragment extends Fragment implements ProjectDetailsNavigator {
  private ProjectDetailsViewModel viewModel;
  private FragmentProjectDetailsBinding binding;
  private CommentAdapter commentAdapter;
  private AppointmentAdapter appointmentAdapter;
  private NavController navController;
  private ProjectDetailsViewModelFactory fac;
  private Project project;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    projectDetailsFragmentArgs args = projectDetailsFragmentArgs.fromBundle(getArguments());
    commentAdapter = new CommentAdapter();
    appointmentAdapter = new AppointmentAdapter();
    project = new Gson().fromJson(args.getProject(), Project.class);
    fac = new ProjectDetailsViewModelFactory(project);
    viewModel = new ViewModelProvider(this, fac).get(ProjectDetailsViewModel.class);
    viewModel.setNavigator(this);
  }

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    binding = FragmentProjectDetailsBinding.inflate(inflater, container, false);
    binding.setLifecycleOwner(this);
    binding.setProjectDetailsViewModel(viewModel);

    binding.recyclerProjectAppointments.setLayoutManager(new LinearLayoutManager(getContext()));
    binding.recyclerProjectAppointments.setHasFixedSize(true);
    appointmentAdapter.setViewModel(viewModel);
    binding.recyclerProjectAppointments.setAdapter(appointmentAdapter);

    return binding.getRoot();
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    navController = Navigation.findNavController(view);
    viewModel
        .getProjectAppointments()
        .observe(
            getViewLifecycleOwner(),
            appointments -> appointmentAdapter.setAppointments(appointments));

    binding.createAppointment.setOnClickListener(
        v -> {
          navController.navigate(
              projectDetailsFragmentDirections.actionProjectDetailsFragmentToAddAppointmentFragment(
                  new Gson().toJson(project)));
        });
    binding.editProject.setOnClickListener(
        v -> {
          navController.navigate(
              projectDetailsFragmentDirections.actionProjectDetailsFragmentToEditProjectFragemnt(
                  new Gson().toJson(project)));
        });
    binding.deleteProject.setOnClickListener(
        v -> {
          viewModel.deleteProject();
        });
    viewModel
        .getDataValid()
        .observe(
            getViewLifecycleOwner(),
            dataValid -> {
              if (dataValid) {
                navController.popBackStack();
              }
            });
  }

  @Override
  public void onResume() {
    super.onResume();
    viewModel.updateData();
  }

  @Override
  public void onItemClick(Appointment appointment) {
    navController.navigate(
        projectDetailsFragmentDirections.actionProjectDetailsFragmentToEditAppointmentFragment(
            new Gson().toJson(appointment, Appointment.class)));
  }
}
