package de.umr.studipro3000.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;

import de.umr.studipro3000.databinding.FragmentUserDetailsBinding;
import de.umr.studipro3000.model.User;
import de.umr.studipro3000.viewmodel.UserDetailsViewModel;

public class userDetailsFragment extends Fragment {
  private UserDetailsViewModel viewModel;
  private FragmentUserDetailsBinding binding;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    viewModel = new ViewModelProvider(this).get(UserDetailsViewModel.class);

    if (getArguments() != null) {
      userDetailsFragmentArgs args = userDetailsFragmentArgs.fromBundle(getArguments());
      viewModel.setUser(new Gson().fromJson(args.getUser(), User.class));
    }
  }

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    binding = FragmentUserDetailsBinding.inflate(inflater, container, false);
    binding.setLifecycleOwner(this);
    binding.setUserDetailsViewModel(viewModel);

    return binding.getRoot();
  }
}
