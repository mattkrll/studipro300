package de.umr.studipro3000.viewmodel;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import de.umr.studipro3000.model.AppointmentType;
import de.umr.studipro3000.model.Project;
import de.umr.studipro3000.model.service.AuthService;
import de.umr.studipro3000.model.service.repository.Repository;
import de.umr.studipro3000.model.service.repository.requests.CreateAppointmentRequest;
import de.umr.studipro3000.model.service.repository.responses.AppointmentResponse;
import de.umr.studipro3000.model.service.repository.responses.AppointmentSuccessResponse;

public class AddAppointmentViewModel extends ViewModel {
  public MutableLiveData<Boolean> dataValid;
  private Repository repository;
  private String appointmentName;
  private String description;
  private String startDate;
  private String endDate;
  private AppointmentType type;
  private ObservableArrayList<String> appointmentTypeSelection;
  private ObservableInt selectedType;
  private Project project;
  private AuthService authService;
  private String errorMessage;

  public AddAppointmentViewModel() {
    this.repository = Repository.getInstance();

    this.authService = AuthService.getInstance();
    this.appointmentName = "";
    this.description = "";
    this.startDate = "";
    this.startDate = "";

    appointmentTypeSelection = new ObservableArrayList<>();
    appointmentTypeSelection.add("DEADLINE");
    appointmentTypeSelection.add("MEETING");
    appointmentTypeSelection.add("GROUP");
    appointmentTypeSelection.add("TEMPLATE");
    selectedType = new ObservableInt(1);
    dataValid = new MutableLiveData<>();
    dataValid.setValue(false);
  }

  public void createAppointment() {
    type = AppointmentType.valueOf(appointmentTypeSelection.get(selectedType.describeContents()));
    CreateAppointmentRequest createAppointmentRequest =
        new CreateAppointmentRequest(appointmentName, description, startDate, endDate, type);
    getResponse(
        repository.appointments(
            project.getId(), authService.currentToken.getValue(), createAppointmentRequest));
  }

  private void getResponse(LiveData<AppointmentResponse> createAppointmentResponse) {
    createAppointmentResponse.observeForever(
        appointmentResponse -> {
          AppointmentSuccessResponse response = appointmentResponse.getAppointmentSuccessResponse();
          if (response != null) {
            dataValid.setValue(true);
          } else {
            if (appointmentResponse.getDefaultErrorResponse() != null) {
              errorMessage = appointmentResponse.getDefaultErrorResponse().getError();
            }
          }
        });
  }

  public MutableLiveData<Boolean> getDataValid() {
    return dataValid;
  }

  public ObservableArrayList<String> getAppointmentTypeSelection() {
    return appointmentTypeSelection;
  }

  public void setAppointmentTypeSelection(ObservableArrayList<String> appointmentTypeSelection) {
    this.appointmentTypeSelection = appointmentTypeSelection;
  }

  public ObservableInt getSelectedType() {
    return selectedType;
  }

  public void setSelectedType(ObservableInt selectedType) {
    this.selectedType = selectedType;
  }

  public AppointmentType getType() {
    return type;
  }

  public void setType(AppointmentType type) {
    this.type = type;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public String getAppointmentName() {
    return appointmentName;
  }

  public void setAppointmentName(String appointmentName) {
    this.appointmentName = appointmentName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }
}
