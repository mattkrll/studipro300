package de.umr.studipro3000.viewmodel;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import de.umr.studipro3000.model.ProjectType;
import de.umr.studipro3000.model.User;
import de.umr.studipro3000.model.service.AuthService;
import de.umr.studipro3000.model.service.repository.Repository;
import de.umr.studipro3000.model.service.repository.requests.ProjectRequest;
import de.umr.studipro3000.model.service.repository.responses.ProjectResponse;
import de.umr.studipro3000.model.service.repository.responses.ProjectSuccessResponse;

public class AdminCreateProjectViewModel extends ViewModel {

  private MutableLiveData<Boolean> projectDataValid;
  private String errorMessage;
  private Repository repository;
  private AuthService authService;
  private String projectName;
  private ProjectType projectType;
  private String projectDescription;
  private ArrayList<User> handlers;
  private ArrayList<User> processors;
  private ObservableArrayList<String> projectTypeSelection;
  private ObservableInt selectedType;

  public AdminCreateProjectViewModel() {
    this.repository = Repository.getInstance();

    this.projectName = "";
    this.projectDescription = "";

    this.projectTypeSelection = new ObservableArrayList<>();
    this.projectTypeSelection.add("FOPRA");
    this.projectTypeSelection.add("SEMINARARBEIT");
    this.projectTypeSelection.add("BACHELORARBEIT");
    this.projectTypeSelection.add("MASTERARBEIT");
    this.projectTypeSelection.add("PROJEKTARBEIT");
    this.projectTypeSelection.add("SWA");

    this.selectedType = new ObservableInt(1);

    this.handlers = new ArrayList<>();

    this.processors = new ArrayList<>();

    this.authService = AuthService.getInstance();

    this.projectDataValid = new MutableLiveData<>();

    this.projectDataValid.setValue(false);
  }

  public void createProject() {
    projectType = ProjectType.valueOf(projectTypeSelection.get(selectedType.describeContents()));
    ProjectRequest projectRequest =
        new ProjectRequest(projectName, projectDescription, handlers, processors, projectType);

    getResponse(repository.initProject(authService.currentToken.getValue(), projectRequest));
  }

  private void getResponse(LiveData<ProjectResponse> adminCreateProjectResponse) {
    adminCreateProjectResponse.observeForever(
        projectResponse -> {
          ProjectSuccessResponse response = projectResponse.getProjectSuccessResponse();
          if (response != null) {
            projectDataValid.setValue(true);
          } else {
            errorMessage = projectResponse.getDefaultErrorResponse().getError();
          }
        });
  }

  public MutableLiveData<Boolean> getProjectDataValid() {
    return projectDataValid;
  }

  public void setProjectDataValid(MutableLiveData<Boolean> projectDataValid) {
    this.projectDataValid = projectDataValid;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public String getProjectName() {
    return projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  public ProjectType getProjectType() {
    return projectType;
  }

  public void setProjectType(ProjectType projectType) {
    this.projectType = projectType;
  }

  public String getProjectDescription() {
    return projectDescription;
  }

  public void setProjectDescription(String projectDescription) {
    this.projectDescription = projectDescription;
  }

  public ArrayList<User> getHandlers() {
    return handlers;
  }

  public void setHandlers(ArrayList<User> handlers) {
    this.handlers = handlers;
  }

  public ArrayList<User> getProcessors() {
    return processors;
  }

  public void setProcessors(ArrayList<User> processors) {
    this.processors = processors;
  }

  public ObservableArrayList<String> getProjectTypeSelection() {
    return projectTypeSelection;
  }

  public void setProjectTypeSelection(ObservableArrayList<String> projectTypeSelection) {
    this.projectTypeSelection = projectTypeSelection;
  }

  public ObservableInt getSelectedType() {
    return selectedType;
  }

  public void setSelectedType(ObservableInt selectedType) {
    this.selectedType = selectedType;
  }
}
