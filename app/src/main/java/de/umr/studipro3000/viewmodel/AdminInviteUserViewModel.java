package de.umr.studipro3000.viewmodel;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import de.umr.studipro3000.model.ProjectRights;
import de.umr.studipro3000.model.UserRole;
import de.umr.studipro3000.model.service.AuthService;
import de.umr.studipro3000.model.service.repository.Repository;
import de.umr.studipro3000.model.service.repository.requests.AddUserInvitationRequest;
import de.umr.studipro3000.model.service.repository.responses.AddUserInviteResponse;

public class AdminInviteUserViewModel extends ViewModel {
  private Repository repository;

  private AuthService authService;
  private String email;
  private UserRole userRole;
  private String projectId;
  private ProjectRights projectRights;

  private MutableLiveData<String> inviteLink;
  private MutableLiveData<Boolean> userInviteIsValid;
  private String errorMessage;

  private ObservableArrayList<String> userRoleSelections;
  private ObservableInt selectedRole;

  private ObservableArrayList<String> projectRightsSelection;
  private ObservableInt selectedRights;

  public AdminInviteUserViewModel() {
    userRoleSelections = new ObservableArrayList<>();
    userRoleSelections.add("Admin");
    userRoleSelections.add("User");
    selectedRole = new ObservableInt(1);

    projectRightsSelection = new ObservableArrayList<>();
    projectRightsSelection.add("Handler");
    projectRightsSelection.add("Processor");
    selectedRole = new ObservableInt(1);

    repository = Repository.getInstance();
    authService = AuthService.getInstance();
    userInviteIsValid = new MutableLiveData<>();
    userInviteIsValid.setValue(false);
    inviteLink = new MutableLiveData<>();
    inviteLink.setValue("");

    email = "";
    projectId = "";
    userRole = UserRole.ROLE_USER;
    projectRights = ProjectRights.HANDLER;
  }

  public void inviteUser() {
    AddUserInvitationRequest addUserInvitationRequest =
        new AddUserInvitationRequest(email, Integer.parseInt(projectId), userRole, projectRights);
    handleResponse(
        repository.addUserInvitation(
            authService.currentToken.getValue(), addUserInvitationRequest));
  }

  public void handleResponse(LiveData<AddUserInviteResponse> inviteUserResponse) {
    inviteUserResponse.observeForever(
        defaultBackendResponse -> {
          AddUserInviteResponse response = inviteUserResponse.getValue();
          if (response.getSuccess().equals("1")) {
            userInviteIsValid.setValue(true);
            inviteLink.setValue(response.getInviteLink());
          } else {
            errorMessage = response.getError();
          }
        });
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public UserRole getUserRole() {
    return userRole;
  }

  public void setUserRole(UserRole userRole) {
    this.userRole = userRole;
  }

  public String getProjectId() {
    return projectId;
  }

  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  public MutableLiveData<Boolean> getUserInviteIsValid() {
    return userInviteIsValid;
  }

  public void setUserInviteIsValid(MutableLiveData<Boolean> userInviteIsValid) {
    this.userInviteIsValid = userInviteIsValid;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public ObservableArrayList<String> getUserRoleSelections() {
    return userRoleSelections;
  }

  public void setUserRoleSelections(ObservableArrayList<String> userRoleSelections) {
    this.userRoleSelections = userRoleSelections;
  }

  public ObservableInt getSelectedRole() {
    return selectedRole;
  }

  public void setSelectedRole(ObservableInt selectedRole) {
    this.selectedRole = selectedRole;
    this.userRole = UserRole.values()[selectedRole.get()];
  }

  public ObservableArrayList<String> getProjectRightsSelection() {
    return projectRightsSelection;
  }

  public void setProjectRightsSelection(ObservableArrayList<String> projectRightsSelection) {
    this.projectRightsSelection = projectRightsSelection;
  }

  public ObservableInt getSelectedRights() {
    return selectedRights;
  }

  public void setSelectedRights(ObservableInt selectedRights) {
    this.selectedRights = selectedRights;
    this.projectRights = ProjectRights.values()[selectedRights.get()];
  }
}
