package de.umr.studipro3000.viewmodel;

import de.umr.studipro3000.model.User;

public interface AdminUserListNavigatior {

  void onItemClick(User user);
}
