package de.umr.studipro3000.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;
import java.util.Objects;

import de.umr.studipro3000.model.User;
import de.umr.studipro3000.model.service.AuthService;
import de.umr.studipro3000.model.service.repository.Repository;
import de.umr.studipro3000.model.service.repository.responses.UserListErrorResponse;
import de.umr.studipro3000.model.service.repository.responses.UserListResponse;

public class AdminUserListViewModel extends ViewModel {
  private Repository repository;
  private AuthService authService;

  private MutableLiveData<List<User>> userListLiveData;
  private AdminUserListNavigatior navigator;

  private String errorMessage;

  public AdminUserListViewModel() {
    this.repository = Repository.getInstance();
    this.authService = AuthService.getInstance();
    this.errorMessage = "";
    userListLiveData = new MutableLiveData<>();
    handleResponse(
        repository.getFetchAllUsers(Objects.requireNonNull(authService.currentToken.getValue())));
  }

  public void updateData() {
    handleResponse(
        repository.getFetchAllUsers(Objects.requireNonNull(authService.currentToken.getValue())));
  };

  private void handleResponse(LiveData<UserListResponse> userListResponseLiveData) {
    userListResponseLiveData.observeForever(
        userListResponse -> {
          List<User> users = userListResponse.getUsers();
          if (users != null) {
            userListLiveData.setValue(users);
          } else {
            UserListErrorResponse userListErrorResponse =
                userListResponse.getUserListErrorResponse();
            if (userListErrorResponse != null) {
              errorMessage = userListErrorResponse.getMessage();
            } else {
              errorMessage = userListResponse.getErrorResponse().getError();
            }
          }
        });
  }

  public void itemClick(User user) {
    navigator.onItemClick(user);
  }

  public void setNavigator(AdminUserListNavigatior navigator) {
    this.navigator = navigator;
  }

  public Repository getRepository() {
    return repository;
  }

  public void setRepository(Repository repository) {
    this.repository = repository;
  }

  public AuthService getAuthService() {
    return authService;
  }

  public void setAuthService(AuthService authService) {
    this.authService = authService;
  }

  public MutableLiveData<List<User>> getUserListLiveData() {
    return userListLiveData;
  }

  public void setUserListLiveData(MutableLiveData<List<User>> userListLiveData) {
    this.userListLiveData = userListLiveData;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
}
