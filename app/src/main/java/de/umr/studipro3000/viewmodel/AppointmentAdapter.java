package de.umr.studipro3000.viewmodel;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import de.umr.studipro3000.R;
import de.umr.studipro3000.databinding.AppointmentItemBinding;
import de.umr.studipro3000.model.Appointment;

public class AppointmentAdapter
    extends RecyclerView.Adapter<AppointmentAdapter.AppointmentViewHolder> {
  private ProjectDetailsViewModel viewModel;

  private List<Appointment> appointments = new ArrayList<>();

  public void setAppointments(List<Appointment> appointments) {
    this.appointments = appointments;
    notifyDataSetChanged();
  }

  public void setViewModel(ProjectDetailsViewModel viewModel) {
    this.viewModel = viewModel;
  }

  @NonNull
  @Override
  public AppointmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    AppointmentItemBinding appointmentItemBinding =
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.getContext()), R.layout.appointment_item, parent, false);
    return new AppointmentAdapter.AppointmentViewHolder(appointmentItemBinding);
  }

  @Override
  public void onBindViewHolder(
      @NonNull AppointmentAdapter.AppointmentViewHolder holder, int position) {
    Appointment currentAppointment = appointments.get(position);
    holder.appointmentItemBinding.setAppointment(currentAppointment);
    holder.appointmentItemBinding.setProjectDetailsViewModel(viewModel);
  }

  @Override
  public int getItemCount() {
    return appointments.size();
  }

  public static class AppointmentViewHolder extends RecyclerView.ViewHolder {
    private AppointmentItemBinding appointmentItemBinding;

    public AppointmentViewHolder(@NonNull AppointmentItemBinding appointmentItemBinding) {
      super(appointmentItemBinding.getRoot());
      this.appointmentItemBinding = appointmentItemBinding;
    }
  }
}
