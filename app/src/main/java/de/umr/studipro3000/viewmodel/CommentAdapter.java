package de.umr.studipro3000.viewmodel;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import de.umr.studipro3000.R;
import de.umr.studipro3000.databinding.CommentItemBinding;
import de.umr.studipro3000.model.Comment;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {
  private ProjectDetailsViewModel viewModel;
  private List<Comment> comments = new ArrayList<>();

  public void setComments(List<Comment> comments) {
    this.comments = comments;
    notifyDataSetChanged();
  }

  public void setViewModel(ProjectDetailsViewModel viewModel) {
    this.viewModel = viewModel;
  }

  @NonNull
  @Override
  public CommentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    CommentItemBinding commentItemBinding =
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.getContext()), R.layout.comment_item, parent, false);

    return new CommentViewHolder(commentItemBinding);
  }

  @Override
  public void onBindViewHolder(@NonNull CommentAdapter.CommentViewHolder holder, int position) {
    Comment currentProject = comments.get(position);
    holder.commentItemBinding.setComment(currentProject);
    holder.commentItemBinding.setProjectDetailsViewModel(viewModel);
  }

  @Override
  public int getItemCount() {
    return comments.size();
  }

  public static class CommentViewHolder extends RecyclerView.ViewHolder {
    private CommentItemBinding commentItemBinding;

    public CommentViewHolder(@NonNull CommentItemBinding commentItemBinding) {
      super(commentItemBinding.getRoot());
      this.commentItemBinding = commentItemBinding;
    }
  }
}
