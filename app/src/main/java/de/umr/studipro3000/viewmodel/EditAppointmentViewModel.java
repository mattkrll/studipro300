package de.umr.studipro3000.viewmodel;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

import de.umr.studipro3000.model.Appointment;
import de.umr.studipro3000.model.AppointmentType;
import de.umr.studipro3000.model.service.AuthService;
import de.umr.studipro3000.model.service.repository.Repository;
import de.umr.studipro3000.model.service.repository.requests.CreateAppointmentRequest;
import de.umr.studipro3000.model.service.repository.responses.AppointmentResponse;
import de.umr.studipro3000.model.service.repository.responses.AppointmentSuccessResponse;
import de.umr.studipro3000.model.service.repository.responses.DefaultBackendResponse;

public class EditAppointmentViewModel extends ViewModel {
  private MutableLiveData<Boolean> dataValid;
  private Repository repository;
  private String appointmentName;
  private String description;
  private String startDate;
  private String endDate;
  private Appointment appointment;
  private AppointmentType type;
  private ObservableArrayList<String> appointmentTypeSelection;
  private ObservableInt selectedType;
  private String errorMessage;
  private AuthService authService;
  private int appointmentId;

  public EditAppointmentViewModel(Appointment appointment) {
    this.repository = Repository.getInstance();
    this.authService = AuthService.getInstance();

    this.appointmentName = appointment.getName();
    this.description = appointment.getDescription();

    SimpleDateFormat sdf;
    sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
    sdf.setTimeZone(TimeZone.getTimeZone("CET"));

    this.startDate = sdf.format(appointment.getStartDate());
    this.endDate = sdf.format(appointment.getEndDate());
    this.type = appointment.getAppointmentType();
    this.appointmentId = appointment.getId();

    appointmentTypeSelection = new ObservableArrayList<>();
    appointmentTypeSelection.add("DEADLINE");
    appointmentTypeSelection.add("MEETING");
    appointmentTypeSelection.add("GROUP");
    appointmentTypeSelection.add("TEMPLATE");
    selectedType = new ObservableInt(1);
    dataValid = new MutableLiveData<>();
    dataValid.setValue(false);
  }

  public void editAppointment() {
    type = AppointmentType.valueOf(appointmentTypeSelection.get(selectedType.describeContents()));
    CreateAppointmentRequest updatedAppointment =
        new CreateAppointmentRequest(appointmentName, description, startDate, endDate, type);
    getResponse(
        repository.appointment_id(
            appointmentId, authService.currentToken.getValue(), updatedAppointment));
  }

  public void deleteAppointment() {
    getDeleteResponse(
        repository.appointment_id(appointmentId, authService.currentToken.getValue()));
  }

  private void getDeleteResponse(LiveData<DefaultBackendResponse> createAppointmentResponse) {
    createAppointmentResponse.observeForever(
        appointmentResponse -> {
          if (appointmentResponse.getSuccess().equals("1")) {
            dataValid.setValue(true);
          } else {
            errorMessage = appointmentResponse.getError();
          }
        });
  }

  private void getResponse(LiveData<AppointmentResponse> createAppointmentResponse) {
    createAppointmentResponse.observeForever(
        appointmentResponse -> {
          AppointmentSuccessResponse response = appointmentResponse.getAppointmentSuccessResponse();
          if (response != null) {
            dataValid.setValue(true);
          } else {
            if (appointmentResponse.getDefaultErrorResponse() != null) {
              errorMessage = appointmentResponse.getDefaultErrorResponse().getError();
            }
          }
        });
  }

  public String getAppointmentName() {
    return appointmentName;
  }

  public void setAppointmentName(String appointmentName) {
    this.appointmentName = appointmentName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public Appointment getAppointment() {
    return appointment;
  }

  public void setAppointment(Appointment appointment) {
    this.appointment = appointment;
  }

  public ObservableArrayList<String> getAppointmentTypeSelection() {
    return appointmentTypeSelection;
  }

  public void setAppointmentTypeSelection(ObservableArrayList<String> appointmentTypeSelection) {
    this.appointmentTypeSelection = appointmentTypeSelection;
  }

  public AppointmentType getType() {
    return type;
  }

  public void setType(AppointmentType type) {
    this.type = type;
  }

  public MutableLiveData<Boolean> getDataValid() {
    return dataValid;
  }

  public void setDataValid(MutableLiveData<Boolean> dataValid) {
    this.dataValid = dataValid;
  }

  public ObservableInt getSelectedType() {
    return selectedType;
  }

  public void setSelectedType(ObservableInt selectedType) {
    this.selectedType = selectedType;
  }
}
