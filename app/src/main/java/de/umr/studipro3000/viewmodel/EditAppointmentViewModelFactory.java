package de.umr.studipro3000.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import de.umr.studipro3000.model.Appointment;

public class EditAppointmentViewModelFactory implements ViewModelProvider.Factory {
  Appointment appointment;

  public EditAppointmentViewModelFactory(Appointment appointment) {
    this.appointment = appointment;
  }

  @NonNull
  @Override
  public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
    if (modelClass.isAssignableFrom(EditAppointmentViewModel.class)) {
      return (T) new EditAppointmentViewModel(appointment);
    }
    throw new IllegalArgumentException("unexpected model class " + modelClass);
  }
}
