package de.umr.studipro3000.viewmodel;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import de.umr.studipro3000.model.Project;
import de.umr.studipro3000.model.ProjectType;
import de.umr.studipro3000.model.User;
import de.umr.studipro3000.model.service.AuthService;
import de.umr.studipro3000.model.service.repository.Repository;
import de.umr.studipro3000.model.service.repository.requests.ProjectRequest;
import de.umr.studipro3000.model.service.repository.responses.ProjectResponse;
import de.umr.studipro3000.model.service.repository.responses.ProjectSuccessResponse;

public class EditProjectViewModel extends ViewModel {
  private MutableLiveData<Boolean> projectDataValid;
  private String errorMessage;
  private Repository repository;
  private AuthService authService;
  private String projectName;
  private ProjectType projectType;
  private String projectDescription;
  private ArrayList<User> handlers;
  private ArrayList<User> processors;
  private ObservableArrayList<String> projectTypeSelection;

  private ObservableInt selectedType;
  private int projectId;

  public EditProjectViewModel(Project project) {
    this.repository = Repository.getInstance();
    this.projectName = project.getName();
    this.projectDescription = project.getDescription();
    projectTypeSelection = new ObservableArrayList<>();
    projectTypeSelection.add("FOPRA");
    projectTypeSelection.add("SEMINARARBEIT");
    projectTypeSelection.add("BACHELORARBEIT");
    projectTypeSelection.add("MASTERARBEIT");
    projectTypeSelection.add("PROJEKTARBEIT");
    projectTypeSelection.add("SWA");
    this.handlers = new ArrayList<>();
    this.processors = new ArrayList<>();
    handlers = project.getHandlers();
    processors = project.getProcessors();
    projectType = project.getProjectType();
    projectId = project.getId();
    selectedType = new ObservableInt(1);

    this.authService = AuthService.getInstance();
    projectDataValid = new MutableLiveData<>();
    projectDataValid.setValue(false);
  }

  public void editProject() {
    projectType = ProjectType.valueOf(projectTypeSelection.get(selectedType.describeContents()));
    ProjectRequest projectRequest =
        new ProjectRequest(projectName, projectDescription, handlers, processors, projectType);
    getResponse(
        repository.update_id(projectId, authService.currentToken.getValue(), projectRequest));
  }

  private void getResponse(LiveData<ProjectResponse> editProjectResponse) {
    editProjectResponse.observeForever(
        editProject -> {
          ProjectSuccessResponse response = editProject.getProjectSuccessResponse();
          if (response != null) {
            projectDataValid.setValue(true);
          } else {
            if (editProject.getDefaultErrorResponse() != null) {
              errorMessage = editProject.getDefaultErrorResponse().getError();
            }
          }
        });
  }

  public MutableLiveData<Boolean> getProjectDataValid() {
    return projectDataValid;
  }

  public void setProjectDataValid(MutableLiveData<Boolean> projectDataValid) {
    this.projectDataValid = projectDataValid;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public String getProjectName() {
    return projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  public ProjectType getProjectType() {
    return projectType;
  }

  public void setProjectType(ProjectType projectType) {
    this.projectType = projectType;
  }

  public String getProjectDescription() {
    return projectDescription;
  }

  public void setProjectDescription(String projectDescription) {
    this.projectDescription = projectDescription;
  }

  public ArrayList<User> getHandlers() {
    return handlers;
  }

  public void setHandlers(ArrayList<User> handlers) {
    this.handlers = handlers;
  }

  public ArrayList<User> getProcessors() {
    return processors;
  }

  public void setProcessors(ArrayList<User> processors) {
    this.processors = processors;
  }

  public ObservableArrayList<String> getProjectTypeSelection() {
    return projectTypeSelection;
  }

  public void setProjectTypeSelection(ObservableArrayList<String> projectTypeSelection) {
    this.projectTypeSelection = projectTypeSelection;
  }

  public ObservableInt getSelectedType() {
    return selectedType;
  }

  public void setSelectedType(ObservableInt selectedType) {
    this.selectedType = selectedType;
  }

  public int getProjectId() {
    return projectId;
  }

  public void setProjectId(int projectId) {
    this.projectId = projectId;
  }
}
