package de.umr.studipro3000.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import de.umr.studipro3000.model.Project;

public class EditProjectViewModelFactory implements ViewModelProvider.Factory {
  Project project;

  public EditProjectViewModelFactory(Project project) {
    this.project = project;
  }

  @NonNull
  @Override
  public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
    if (modelClass.isAssignableFrom(EditProjectViewModel.class)) {
      return (T) new EditProjectViewModel(project);
    }
    throw new IllegalArgumentException("unexpected model class " + modelClass);
  }
}
