package de.umr.studipro3000.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import de.umr.studipro3000.model.service.repository.Repository;
import de.umr.studipro3000.model.service.repository.requests.CreateByInviteCodeRequest;
import de.umr.studipro3000.model.service.repository.responses.DefaultBackendResponse;

public class EnterInviteCodeAndUserDataViewModel extends ViewModel {
  private Repository repository;

  private String inviteCode;
  private String forname;
  private String name;
  private String studentNumber;
  private String password;
  private String username;
  private String courseOfStudy;
  private String examinationRegulations;

  private MutableLiveData<Boolean> inviteCodeIsValid;
  private String errorMessage;

  public EnterInviteCodeAndUserDataViewModel() {
    this.repository = Repository.getInstance();

    this.inviteCodeIsValid = new MutableLiveData<>();
    this.inviteCodeIsValid.setValue(false);

    this.inviteCode = "";
    this.forname = "";
    this.name = "";
    this.studentNumber = "";
    this.password = "";
    this.username = "";
    this.courseOfStudy = "";
    this.examinationRegulations = "";
  }

  public void createUserWithCode() {
    CreateByInviteCodeRequest createByInviteCodeRequest =
        new CreateByInviteCodeRequest(
            forname,
            name,
            Integer.parseInt(studentNumber),
            password,
            username,
            courseOfStudy,
            examinationRegulations);

    handleResponse(repository.addUserByInvitation(inviteCode, createByInviteCodeRequest));
  }

  private void handleResponse(LiveData<DefaultBackendResponse> createUserByInviteCodeResponse) {
    createUserByInviteCodeResponse.observeForever(
        defaultBackendResponse -> {
          DefaultBackendResponse response = createUserByInviteCodeResponse.getValue();

          if (response.getSuccess().equals("1")) {
            inviteCodeIsValid.setValue(true);
          } else {
            errorMessage = response.getError();
          }
        });
  }

  public String getInviteCode() {
    return inviteCode;
  }

  public void setInviteCode(String inviteCode) {
    this.inviteCode = inviteCode;
  }

  public String getForname() {
    return forname;
  }

  public void setForname(String forname) {
    this.forname = forname;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getStudentNumber() {
    return studentNumber;
  }

  public void setStudentNumber(String studentNumber) {
    this.studentNumber = studentNumber;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getCourseOfStudy() {
    return courseOfStudy;
  }

  public void setCourseOfStudy(String courseOfStudy) {
    this.courseOfStudy = courseOfStudy;
  }

  public String getExaminationRegulations() {
    return examinationRegulations;
  }

  public void setExaminationRegulations(String examinationRegulations) {
    this.examinationRegulations = examinationRegulations;
  }

  public MutableLiveData<Boolean> getInviteCodeIsValid() {
    return inviteCodeIsValid;
  }

  public void setInviteCodeIsValid(MutableLiveData<Boolean> inviteCodeIsValid) {
    this.inviteCodeIsValid = inviteCodeIsValid;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
}
