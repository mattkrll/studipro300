package de.umr.studipro3000.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import de.umr.studipro3000.model.service.AuthService;

public class LoginViewModel extends ViewModel {
  private final AuthService authService;

  private MutableLiveData<AuthService.AuthenticationState> authenticationState;
  private MutableLiveData<String> errorMessage;
  private String usernameEmail;
  private String password;

  public LoginViewModel() {
    authService = AuthService.getInstance();
    authenticationState = authService.authenticationState;
    errorMessage = authService.errorMessage;
    usernameEmail = "";
    password = "";
  }

  public void signIn() {
    authService.signIn(usernameEmail, password);
  }

  public MutableLiveData<AuthService.AuthenticationState> getAuthenticationState() {
    return authenticationState;
  }

  public MutableLiveData<String> getErrorMessage() {
    return errorMessage;
  }

  public String getUsernameEmail() {
    return usernameEmail;
  }

  public void setUsernameEmail(String usernameEmail) {
    this.usernameEmail = usernameEmail;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
