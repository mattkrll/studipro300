package de.umr.studipro3000.viewmodel;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import de.umr.studipro3000.R;
import de.umr.studipro3000.databinding.ProjectItemBinding;
import de.umr.studipro3000.model.Project;
import de.umr.studipro3000.model.User;

public class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.ProjectViewHolder>
    implements Filterable {
  private ProjectOverviewViewModel viewModel;
  private List<Project> projects = new ArrayList<>();
  private List<Project> projectsFull = new ArrayList<>();
  private Filter projectFilter =
      new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
          List<Project> filteredProjects = new ArrayList<>();
          if (charSequence == null | charSequence.length() == 0) {
            filteredProjects.addAll(projectsFull);
          } else {
            String filterPattern = charSequence.toString().toLowerCase().trim();
            for (Project project : projectsFull) {
              if (project.getName().toLowerCase().contains(filterPattern)) {
                filteredProjects.add(project);
              }

              if (project.getProjectType().toString().toLowerCase().contains(filterPattern)) {
                if (!filteredProjects.contains(project)) {
                  filteredProjects.add(project);
                }
              }

              for (User handler : project.getHandlers()) {
                if (handler.getEmail().toLowerCase().contains(filterPattern))
                  if (!filteredProjects.contains(project)) {
                    filteredProjects.add(project);
                  }
              }

              for (User processor : project.getProcessors()) {
                if (processor.getEmail().toLowerCase().contains(filterPattern))
                  if (!filteredProjects.contains(project)) {
                    filteredProjects.add(project);
                  }
              }
            }
          }

          FilterResults results = new FilterResults();
          results.values = filteredProjects;
          return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
          projects.clear();
          projects.addAll((List) filterResults.values);
          notifyDataSetChanged();
        }
      };

  public void setProjects(List<Project> projects) {
    this.projects = projects;
    projectsFull = new ArrayList<>(projects);
    notifyDataSetChanged();
  }

  public void setViewModel(ProjectOverviewViewModel viewModel) {
    this.viewModel = viewModel;
  }

  @Override
  public Filter getFilter() {
    return projectFilter;
  }

  @NonNull
  @Override
  public ProjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    ProjectItemBinding projectItemBinding =
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.getContext()), R.layout.project_item, parent, false);

    return new ProjectViewHolder(projectItemBinding);
  }

  @Override
  public void onBindViewHolder(@NonNull ProjectViewHolder holder, int position) {
    Project currentProject = projects.get(position);
    holder.projectItemBinding.setProject(currentProject);
    holder.projectItemBinding.setProjectOverviewViewModel(viewModel);
  }

  @Override
  public int getItemCount() {
    return projects.size();
  }

  public static class ProjectViewHolder extends RecyclerView.ViewHolder {
    private ProjectItemBinding projectItemBinding;

    public ProjectViewHolder(@NonNull ProjectItemBinding projectItemBinding) {
      super(projectItemBinding.getRoot());
      this.projectItemBinding = projectItemBinding;
    }
  }
}
