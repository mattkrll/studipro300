package de.umr.studipro3000.viewmodel;

import de.umr.studipro3000.model.Appointment;

public interface ProjectDetailsNavigator {
  void onItemClick(Appointment appointment);
}
