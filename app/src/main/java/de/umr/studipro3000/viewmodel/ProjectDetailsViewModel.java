package de.umr.studipro3000.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;
import java.util.Objects;

import de.umr.studipro3000.model.Appointment;
import de.umr.studipro3000.model.Project;
import de.umr.studipro3000.model.service.AuthService;
import de.umr.studipro3000.model.service.repository.Repository;
import de.umr.studipro3000.model.service.repository.responses.AppointmentListErrorResponse;
import de.umr.studipro3000.model.service.repository.responses.AppointmentListResponse;
import de.umr.studipro3000.model.service.repository.responses.ProjectResponse;
import de.umr.studipro3000.model.service.repository.responses.ProjectSuccessResponse;

public class ProjectDetailsViewModel extends ViewModel {
  private Repository repository;
  private AuthService authService;
  private Project project;
  private String errorMessage;
  private ProjectDetailsNavigator navigator;
  private MutableLiveData<List<Appointment>> appointmentsLiveData;
  private MutableLiveData<Boolean> dataValid;

  public ProjectDetailsViewModel(Project project) {
    this.project = project;
    this.repository = Repository.getInstance();
    this.authService = AuthService.getInstance();
    this.errorMessage = "";
    appointmentsLiveData = new MutableLiveData<>();
    dataValid = new MutableLiveData<>();
    dataValid.setValue(false);

    handleResponse(
        repository.appointments(
            project.getId(), Objects.requireNonNull(authService.currentToken.getValue())));
  }

  public void updateData() {
    handleResponse(
        repository.appointments(
            project.getId(), Objects.requireNonNull(authService.currentToken.getValue())));
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public void deleteProject() {
    getResponse(
        repository.delete_id(
            project.getId(), Objects.requireNonNull(authService.currentToken.getValue())));
  }

  private void handleResponse(LiveData<AppointmentListResponse> appointmentResponseLiveData) {
    appointmentResponseLiveData.observeForever(
        appointmentResponse -> {
          List<Appointment> appointments = appointmentResponse.getAppointments();

          if (appointments != null) {
            appointmentsLiveData.setValue(appointments);
          } else {
            AppointmentListErrorResponse appointmentErrorResponse =
                appointmentResponse.getAppointmentErrorResponse();
            if (appointmentErrorResponse != null) {
              errorMessage = appointmentErrorResponse.getMessage();
            } else {
              errorMessage = appointmentResponse.getDefaultErrorResponse().getError();
            }
          }
        });
  }

  private void getResponse(LiveData<ProjectResponse> deleteProjectResponse) {
    deleteProjectResponse.observeForever(
        delete -> {
          ProjectSuccessResponse response = delete.getProjectSuccessResponse();
          if (response != null) {
            dataValid.setValue(true);
          } else {
            if (delete.getDefaultErrorResponse() != null) {
              errorMessage = delete.getDefaultErrorResponse().getError();
            }
          }
        });
  }

  public void logout() {
    authService.signOut();
  }

  public void setNavigator(ProjectDetailsNavigator projectDetailsNavigator) {
    this.navigator = projectDetailsNavigator;
  }

  public void itemClick(Appointment appointment) {
    navigator.onItemClick(appointment);
  }

  public MutableLiveData<List<Appointment>> getProjectAppointments() {
    return appointmentsLiveData;
  }

  public void setAppointmentsLiveData(MutableLiveData<List<Appointment>> appointmentsLiveData) {
    this.appointmentsLiveData = appointmentsLiveData;
  }

  public Repository getRepository() {
    return repository;
  }

  public void setRepository(Repository repository) {
    this.repository = repository;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public MutableLiveData<Boolean> getDataValid() {
    return dataValid;
  }

  public void setDataValid(MutableLiveData<Boolean> dataValid) {
    this.dataValid = dataValid;
  }
}
