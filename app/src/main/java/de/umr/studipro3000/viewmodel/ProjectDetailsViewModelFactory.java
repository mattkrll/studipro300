package de.umr.studipro3000.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import de.umr.studipro3000.model.Project;

public class ProjectDetailsViewModelFactory implements ViewModelProvider.Factory {
  Project project;

  public ProjectDetailsViewModelFactory(Project project) {
    this.project = project;
  }

  @NonNull
  @Override
  public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
    if (modelClass.isAssignableFrom(ProjectDetailsViewModel.class)) {
      return (T) new ProjectDetailsViewModel(project);
    }
    throw new IllegalArgumentException("unexpected model class " + modelClass);
  }
}
