package de.umr.studipro3000.viewmodel;

import de.umr.studipro3000.model.Project;

public interface ProjectOverViewNavigator {

  void onItemClick(Project project);
}
