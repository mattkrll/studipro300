package de.umr.studipro3000.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import de.umr.studipro3000.model.Project;
import de.umr.studipro3000.model.UserRole;
import de.umr.studipro3000.model.service.AuthService;
import de.umr.studipro3000.model.service.repository.Repository;
import de.umr.studipro3000.model.service.repository.responses.ProjectListResponse;
import de.umr.studipro3000.model.service.repository.responses.ProjectListSuccessResponse;

public class ProjectOverviewViewModel extends ViewModel {
  private Repository repository;
  private AuthService authService;

  private MutableLiveData<List<Project>> projectsLiveData;
  private ArrayList<UserRole> currentUserRoles;

  private String errorMessage;

  private ProjectOverViewNavigator navigator;

  public ProjectOverviewViewModel() {
    this.repository = Repository.getInstance();
    this.authService = AuthService.getInstance();
    this.currentUserRoles = authService.currentUser.getValue().getRoles();
    this.errorMessage = "";
    this.projectsLiveData = new MutableLiveData<>();
    handleResponse(
        currentUserRoles.contains(UserRole.ROLE_ADMIN)
            ? repository.getAllprojects((authService.currentToken.getValue()))
            : repository.getProjectsOfUser(authService.currentToken.getValue()));
  }

  public void setNavigator(ProjectOverViewNavigator navigator) {
    this.navigator = navigator;
  }

  public void updateData() {
    handleResponse(
        currentUserRoles.contains(UserRole.ROLE_ADMIN)
            ? repository.getAllprojects((authService.currentToken.getValue()))
            : repository.getProjectsOfUser(authService.currentToken.getValue()));
  }

  private void handleResponse(LiveData<ProjectListResponse> projectListResponseLiveData) {
    projectListResponseLiveData.observeForever(
        projectListResponse -> {
          ProjectListSuccessResponse projectListSuccessResponse =
              projectListResponse.getProjectListSuccessResponse();

          if (projectListSuccessResponse != null) {
            projectsLiveData.setValue(projectListSuccessResponse.getResult());
            System.out.println(projectsLiveData.getValue().toString());
          } else {
            errorMessage = projectListResponse.getErrorResponse().getError();
          }
        });
  }

  public void itemClick(Project project) {
    navigator.onItemClick(project);
  }

  public void logout() {
    authService.signOut();
  }

  public MutableLiveData<List<Project>> getProjectsLiveData() {
    return projectsLiveData;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public ArrayList<UserRole> getCurrentUserRoles() {
    return currentUserRoles;
  }

  public void setCurrentUserRoles(ArrayList<UserRole> currentUserRoles) {
    this.currentUserRoles = currentUserRoles;
  }
}
