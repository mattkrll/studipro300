package de.umr.studipro3000.viewmodel;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import de.umr.studipro3000.R;
import de.umr.studipro3000.databinding.UserItemBinding;
import de.umr.studipro3000.model.User;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder>
    implements Filterable {

  AdminUserListViewModel viewModel;
  private List<User> users = new ArrayList<>();
  private List<User> usersFull = new ArrayList<>();
  private Filter userFilter =
      new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
          List<User> filteredUsers = new ArrayList<>();
          if (charSequence == null | charSequence.length() == 0) {
            filteredUsers.addAll(usersFull);
          } else {
            String filterPattern = charSequence.toString().toLowerCase().trim();
            for (User user : usersFull) {
              if (user.getUsername().toLowerCase().contains(filterPattern)) {
                filteredUsers.add(user);
              }
            }
          }
          FilterResults results = new FilterResults();
          results.values = filteredUsers;
          return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
          users.clear();
          users.addAll((List) filterResults.values);
          notifyDataSetChanged();
        }
      };

  public void setUsers(List<User> users) {
    this.users = users;
    usersFull = new ArrayList<>(users);
    notifyDataSetChanged();
  }

  public void setViewModel(AdminUserListViewModel viewModel) {
    this.viewModel = viewModel;
  }

  @NonNull
  @Override
  public UserAdapter.UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    UserItemBinding userItemBinding =
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.getContext()), R.layout.user_item, parent, false);

    return new UserAdapter.UserViewHolder(userItemBinding);
  }

  @Override
  public void onBindViewHolder(@NonNull UserAdapter.UserViewHolder holder, int position) {
    User currentUser = users.get(position);
    holder.userItemBinding.setUser(currentUser);
    holder.userItemBinding.setAdminUserListViewModel(viewModel);
  }

  @Override
  public int getItemCount() {
    return users.size();
  }

  @Override
  public Filter getFilter() {
    return userFilter;
  }

  public static class UserViewHolder extends RecyclerView.ViewHolder {
    private UserItemBinding userItemBinding;

    public UserViewHolder(@NonNull UserItemBinding userItemBinding) {
      super(userItemBinding.getRoot());
      this.userItemBinding = userItemBinding;
    }
  }
}
