package de.umr.studipro3000.viewmodel;

import androidx.lifecycle.ViewModel;

import de.umr.studipro3000.model.User;

public class UserDetailsViewModel extends ViewModel {
  private User user;

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
